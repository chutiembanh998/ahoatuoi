<?php

namespace App\Services;

use App\Models\News;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class NewsService extends BaseService
{
    public function index(){
       $news = News::orderBy('id','DESC')->get();
       return $news;
    }

    public function edit($id){
       $news = News::where('id',$id)->first();
       return $news;
    }

    public function updateSeo($request,$id){
        $data = [
            'title_seo' => $request->title_seo,
            'desc_seo' => $request->desc_seo,
           'key_seo' => $request->key_seo,
        ];

        News::where('id',$id)->update($data);
        return true;
    }

    public function store($request){
        $id = $request->id;
        $title = $request->title;
        $description = $request->description;
        $content = $request->content;
        $avatar = '';
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('avatars');
            $img = Image::make($file)->fit(300, 300);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = News::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }

        $data = [
            'title' => $title,
            'slug' => Str::slug($title),
            'description' => $description,
            'content' => $content,
            'avatar' => $avatar,
        ];
        News::query()->updateOrCreate(
            ['id' => $id],
            $data
        );
        return true;
    }

    public function delete($id){
        News::where('id',$id)->delete();
        return true;
    }
}
