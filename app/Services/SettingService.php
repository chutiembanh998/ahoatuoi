<?php

namespace App\Services;

use App\Models\Setting;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class SettingService extends BaseService
{
    public function index(){
        $setting = Setting::first();
        return $setting;
    }

    public function updateSeo($request){

        $data = [
            'title_seo' => $request->title_seo,
            'desc_seo' => $request->desc_seo,
           'key_seo' => $request->key_seo,
        ];

        Setting::first()->update($data);
        return true;
    }
}
