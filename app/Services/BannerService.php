<?php

namespace App\Services;

use App\Models\Banner;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
class BannerService extends BaseService
{
    public function index(){
        $banners = Banner::get();
        return $banners;
    }

    public function edit($id){
        $banner = Banner::where('id',$id)->first();
        return $banner;
    }

    public function store($request){
        $id = $request->id;
        $title = $request->title;
        $type = $request->type;
        $link = $request->link;
        $avatar = '';
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('avatars');
            $img = Image::make($file);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Banner::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }

        $data = [
            'type' => $type,
            'title' => $title,
            'link' => $link,
            'avatar' => $avatar,
        ];
        Banner::query()->updateOrCreate(
            ['id' => $id],
            $data
        );
        return true;
    }

    public function delete($id){
        Banner::where('id',$id)->delete();
        return true;
    }
}
