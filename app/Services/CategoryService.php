<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class CategoryService extends BaseService
{
    public function index(){
        $categories = Category::get();
        return $categories;
    }

    public function listProduct(){
        $categories = Category::with('product')->get();
        return $categories;
    }

    public function edit($id){
        $category = Category::where('id',$id)->first();
        return $category;
    }

    public function store($request){
        $id = $request->id;
        $name = $request->name;
        $status = $request->status;
        $avatar = '';
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('avatars');
            $img = Image::make($file);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Category::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }

        $data = [
            'name' => $name,
            'slug' => Str::slug($name),
            'status' => $status,
            'avatar' => $avatar,
        ];
        Category::query()->updateOrCreate(
            ['id' => $id],
            $data
        );
        return true;
    }

    public function updateSeo($request,$id){

        $data = [
            'title_seo' => $request->title_seo,
            'desc_seo' => $request->desc_seo,
            'key_seo' => $request->key_seo,
        ];

        Category::where('id',$id)->update($data);
        return true;
    }

    public function delete($id){
        Category::where('id',$id)->delete();
        return true;
    }
}
