<?php

namespace App\Services;

use App\Models\TransportFee;

class FeeService extends BaseService
{
    public function index(){
        $banners = TransportFee::get();
        return $banners;
    }

    public function edit($id){
        $banner = TransportFee::where('id',$id)->first();
        return $banner;
    }

    public function store($request){
        $id = $request->id;
        $data = [
            'district' => $request->district,
            'fee' => $request->fee,
        ];

        TransportFee::query()->updateOrCreate(
            ['id' => $id],
            $data
        );
        return true;
    }

    public function delete($id){
        TransportFee::where('id',$id)->delete();
        return true;
    }
}
