<?php

namespace App\Services;

use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class ProductService extends BaseService
{
    public function index(){
       $products = Product::with('category')->orderBy('id','DESC')->get();
       return $products;
    }

    public function edit($id){
       $product = Product::where('id',$id)->first();
       return $product;
    }

    public function updateSeo($request,$id){
        $data = [
            'title_seo' => $request->title_seo,
            'desc_seo' => $request->desc_seo,
           'key_seo' => $request->key_seo,
        ];

        Product::where('id',$id)->update($data);
        return true;
    }

    public function store($request){
        $id = $request->id;
        $title = $request->title;
        $description = $request->description;
        $content = $request->content;
        $code = $request->code;
        $price = $request->price;
        $promt_price = $request->promt_price;
        $status = $request->status;
        $category = $request->category;
        $avatar = '';
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('avatars');
            $img = Image::make($file)->fit(300, 400);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Product::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }

        $data = [
            'title' => $title,
            'code' => $code,
            'slug' => Str::slug($title),
            'description' => $description,
            'content' => $content,
            'avatar' => $avatar,
            'price' => $price,
            'promt_price' => $promt_price,
            'status' => $status,
        ];
        $product = Product::query()->updateOrCreate(
            ['id' => $id],
            $data
        );
        $product->save();
        $categories = explode(',', $category);

        foreach($categories as $row){
            $category = Category::find($row);
            (isset($id)) ?  $product->category()->sync($category) : $product->category()->attach($category) ;
        }
        return true;
    }

    public function delete($id){
        Product::where('id',$id)->delete();
        CategoryProduct::where('product_id',$id)->delete();
        return true;
    }
}
