<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function tempNews(){
        return view('site.news');
    }

    public function getNews(Request $request){
        if($request->ajax()){
            $key = $request->key;
            $news = News::where('show',1)->where('title','LIKE','%'.$key.'%')->paginate(8);
            return view('site.load-ajax.load-news', compact('news'))->render();
        }
    }

    public function tempNewsDetail($slug){
        $news = News::where('slug',$slug)->first();
        $reles = News::where('slug','<>',$slug)->orderBy('id','DESC')->get();
        return view('site.news_detail', compact('news','reles'));
    }
    
}
