<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\TransportFee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Http;

class CheckoutController extends Controller
{
    protected $http;
    function __construct()
    {
        $this->http = Http::timeout(30);
    }

    public function index(){
        $total = Session::get('total-flower', []);
        $transports = TransportFee::get();
        return view('site.checkout', compact('total','transports'));
    }

    public function store(Request $request){
        return DB::transaction( function() use($request){
            $website = 'http://annhienflowers.vn';
            $data = $request->input();
            $carts = Session::get('cart-flower', []);
            $total = Session::get('total-flower', []);

            $data['code'] = 'HT'.time();
            $data['status'] = 0;
            $data['total_price'] = $total['total_price'];

            $order = Order::create($data);
            $order->save();

            foreach($carts as $key => $item){
                $data_dt = [
                    'order_id' => $order->id,
                    'product_id' => $key,
                    'code' => $item['code'] ?? null,
                    'title' => $item['title'],
                    'slug' => $item['slug'],
                    'image' => $item['avatar'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                ];
                $result = OrderDetail::create($data_dt);
                $data['website'] = 'ahoatuoi.com';
                if($result){
                    $data_total = [
                        'data' => $data,
                        'cart' => $carts,
                        'total' => $total,
                    ];
                    $this->http->post($website.'/api/order/store', $data_total);
                    Session::flush('total-flower');
                    Session::flush('cart-flower');
                    return redirect()->route('checkout.invoice',['code' => $data['code']]);
                }
            }
        });
    }

    public function invoice($code){
        $bill = Order::where('code',$code)->with('detail')->first();
        return view('site.invoice', compact('bill'));
    }
}
