<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
class LoginController extends Controller
{
    public function redirectToProvider($driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    public function handleProviderCallback($driver)
    {
        try {
            $user = Socialite::driver($driver)->user();
        } catch (\Exception $e) {
            return redirect()->route('dashboard');
        }

        $existingUser = User::where('email', $user->getEmail())->first();

        if ($existingUser) {
            auth()->login($existingUser, true);
            return redirect()->route('dashboard');
        } else {
            $newUser                    = new User;
            $newUser->provider_name     = $driver;
            $newUser->provider_id       = $user->getId();
            $newUser->name              = $user->getName();
            $newUser->email             = $user->getEmail();
            $newUser->email_verified_at = now();
            $newUser->avatar            = $user->getAvatar();
            $newUser->save();

            auth()->login($newUser, true);
            $newUser->assignRole('member');
        }

        return redirect()->route('dashboard');
    }
    public function optimize()
    {
        Artisan::call('optimize');
        return 'optimize';
    }
    public function cache()
    {
        Artisan::call('cache:clear');
        return 'cache';
    }
    public function config()
    {
        Artisan::call('config:cache');
        return 'config';
    }
    public function storage()
    {
        Artisan::call('storage:link');
        return 'storage';
    }
    public function migrate()
    {
        Artisan::call('migrate');
        return 'migrate';
    }
}
