<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Banner;
use DB;
class ProductController extends Controller
{
    // get template product
    public function tempProducts($slug){
        
        $category = Category::where('show',Category::show['show'])->where('slug',$slug)->first();
        $banner = Banner::where('type',Banner::type['products_banner'])->where('show',Banner::show['show'])->first();
        //search, tags, price, catalog
        $categories = Category::where('show',Category::show['show'])->get();
        return view('site.products', compact('categories','category','banner'));
    }
    // get products 
    public function getProducts(Request $request){
        if($request->ajax()){
            $query = DB::table('products as p')
            ->rightJoin('category_product as cp', function($q){
                $q->on('cp.product_id','=','p.id');
            })
            ->leftJoin('categories as c', function($q){
                $q->on('c.id','=','cp.category_id');
            })
            ->where('c.id',$request->id)
            ->where('p.show',Product::show['show']);
            if($request->search){
                $query->where('title', 'LIKE', "%{$request->search}%");
            }
            if($request->sort == 'new'){
                $query->orderBy('id','DESC');
            }
            if($request->sort == 'price-up'){
                $query->orderBy('price','ASC');
            }
            if($request->sort == 'price-down'){
                $query->orderBy('price','DESC');
            }
            $products = $query->selectRaw('p.*')->paginate($request->limit); 
        }
        return view('site.load-ajax.load-products', compact('products'))->render();
    }
    // get product detail modal
    public function productDetailModal(Request $request){
        if($request->ajax()){
            $id = $request->id;
            $product = Product::where('id',$id)->where('show',Product::show['show'])->first();
            return response()->json($product);
        }
    }
    // get template product detail 
    public function tempProductDetail($slug){
        $product = Product::with('category')->where('slug',$slug)->where('show',Product::show['show'])->first();
        $ids = [];
        foreach($product->category as $row){
            $ids[] = $row->id;
        }
        $rela_products = Product::with(['category' => function($q) use($ids){
            $q->whereIn('category_id',$ids);
        }])->where('slug','!=',$slug)->orderBy('id','DESC')->limit(15)->get(); 
        return view('site.product_detail',compact('product','rela_products'));
    }

}
