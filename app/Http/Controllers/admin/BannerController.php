<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;

class BannerController extends Controller
{
    public function index(){
        $banners = Banner::orderBy('id','desc')->paginate(10);
        return view('admin.banner.index', compact('banners'));
    }

    public function tempStore(){
        return view('admin.banner.store');
    }

    public function store(Request $request){
        $id = $request->id;
        $title = $request->title;
        $status = $request->status;
        $link = $request->link;
        $type = $request->type ;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Banner::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }
        
        $data_info = [
            'title' => $title,
            'show' => $status,
            'type' => $type,
            'avatar' => $avatar,
            'link' => $link
        ];
        Banner::query()->updateOrCreate(
            ['id' => $id],
            $data_info
        );
        return redirect()->route('banner.list');
    }

    public function edit($id){
        $banner = Banner::where('id',$id)->first();
        return view('admin.banner.edit', compact('banner'));
    }

    public function delete($id){
        Banner::where('id',$id)->delete();
        return redirect()->route('banner.list');
    }
}

