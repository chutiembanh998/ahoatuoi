<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;

class SettingController extends Controller
{
    public function index(){
        $setting = Setting::first();
        return view('admin.setting', compact('setting'));
    }

    public function store(Request $request){
        $data = $request->input();
        unset($data['_token']);
        if ($request->hasFile('favicon')) {
            $file = $request->file('favicon');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file);
            Storage::put($path, (string) $img->encode());   
            $data['favicon'] = Storage::url($path);
        }else{
            $check = Setting::first();
            $data['favicon'] = (isset($check)) ? $check->avatar : 'default.webp';
        }

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file);
            Storage::put($path, (string) $img->encode());   
            $data['logo'] = Storage::url($path);
        }else{
            $check = Setting::first();
            $data['logo'] = (isset($check)) ? $check->avatar : 'default.webp';
        }

        Setting::where('id',1)->update($data);

        return redirect()->route('setting.list');

    }
}
