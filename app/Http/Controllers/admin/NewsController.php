<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class NewsController extends Controller
{
    public function index(){
        $news = News::orderBy('id','DESC')->paginate(20);
        return view('admin.news.index', compact('news'));
    } 

    public function tempStore(){
        return view('admin.news.store');
    }

    public function store(Request $request){
        $id = $request->id;
        $title = $request->title;
        $description = $request->description;
        $content = $request->content;
        $status = $request->status ;
        $title_seo = $request->title_seo ;
        $desc_seo = $request->desc_seo;
        $key_seo = $request->key_seo;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file)->fit(300, 300);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = News::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }
        
        $data_info = [
            'title' => $title,
            'slug' => Str::slug($title),
            'description' => $description,
            'content' => $content,
            'avatar' => $avatar,
            'status' => $status,
            'title_seo' => $title_seo,
            'desc_seo' => $desc_seo,
            'key_seo' => $key_seo,
        ];
        News::query()->updateOrCreate(
            ['id' => $id],
            $data_info
        );
        return redirect()->route('news.list');
    }

    public function edit($id){
        $news = News::where('id',$id)->first();
        return view('admin.news.edit', compact('news'));
    }

    public function delete($id){
        News::where('id',$id)->delete();
        return redirect()->route('news.list');
    }
}
