<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleController extends Controller
{
    public function cawlHtml(){
         // METHOD 2: USING SIMPLE HTML DOM (Not recommended, against Google)
         $html = file_get_html('http://www.google.com/search?q='.urlencode($search).'&start='.$page.'&lr=lang_vi&cr=countryVN');
         $stacks = array();
         $exclude = 'google.com';
         foreach($html->find('a[href^=/url?q]') as $element) {
             $url = $element->href;
             $url = @str_replace("/url?q=","",$url);
             $url = @explode('&amp;', $url)[0];

             if (strpos($url, $exclude)) continue;
             $url = urldecode($url);
             array_push($stacks, $url);
         }

         $result['url'] = $stacks;
         // dd($stacks);die;

         // check link
         $arr_matching = array();
         if(isset($request->arr_link)){
             $matchings = $request->arr_link;
             foreach($matchings as $ma){
                 $links = explode('?',$ma);
                 array_push($arr_matching, $links[0]);
             }
             $stacks = array_diff($stacks, $arr_matching);
         }
         // end

     return response()->json($result);
}
}
