<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategoryProduct;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;

class ProductController extends Controller
{
    public function index(){
        $products = Product::with('category')->orderBy('id','DESC')->paginate(20);
        return view('admin.product.index', compact('products'));
    } 

    public function tempStore(){
        $categories = Category::where('show',Category::show['show'])->get();
        return view('admin.product.store', compact('categories'));
    }

    public function store(Request $request){
        $id = $request->id;
        $title = $request->title;
        $category = $request->category;
        $description = $request->description;
        $content = $request->content;
        $price = $request->price;
        $promt_price = $request->promt_price ;
        $title_seo = $request->title_seo ;
        $desc_seo = $request->desc_seo;
        $key_seo = $request->key_seo;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file)->fit(300, 300);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Product::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }
        
        $data_info = [
            'code' => 'HT'.time(),
            'title' => $title,
            'slug' => Str::slug($title),
            'description' => $description,
            'content' => $content,
            'avatar' => $avatar,
            'price' => $price,
            'promt_price' => $promt_price,
            'title_seo' => $title_seo,
            'desc_seo' => $desc_seo,
            'key_seo' => $key_seo,
            'status' => 1,
        ];
        $product = Product::query()->updateOrCreate(
            ['id' => $id],
            $data_info
        );
        $product->save();

        $categories = explode(',', $category);

        foreach($categories as $row){
            $category = Category::find($row);
            (isset($id)) ?  $product->category()->sync($category) : $product->category()->attach($category) ;
        }

        return redirect()->route('product.list');
    }

    public function edit($id){
        $product = Product::where('id',$id)->with('category')->first();
        $categories = Category::where('show',Category::show['show'])->get();
        return view('admin.product.edit', compact('product','categories'));
    }

    public function delete($id){
        Product::where('id',$id)->delete();
        CategoryProduct::where('product_id',$id)->delete();
        return redirect()->route('product.list');
    }
}