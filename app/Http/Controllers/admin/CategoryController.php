<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as Image;
class CategoryController extends Controller
{
    public function index(){
        $categories = Category::withCount('product')->orderBy('id','DESC')->paginate(20);
        return view('admin.category.index',compact('categories'));
    }

    public function tempStore(){
        return view('admin.category.store');
    }

    public function store(Request $request){
        $id = $request->id;
        $name = $request->name;
        $status = $request->status;
        $title_seo = $request->title_seo ;
        $desc_seo = $request->desc_seo;
        $key_seo = $request->key_seo;
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $path = $file->hashName('public/avatars');
            $img = Image::make($file)->fit(300, 300);
            Storage::put($path, (string) $img->encode());   
            $avatar = Storage::url($path);
        }else{
            $check = Category::where('id',$id)->first();
            $avatar = (isset($check)) ? $check->avatar : 'default.webp';
        }
        
        $data_info = [
            'name' => $name,
            'slug' => Str::slug($name),
            'status' => $status,
            'avatar' => $avatar,
            'title_seo' => $title_seo,
            'desc_seo' => $desc_seo,
            'key_seo' => $key_seo,
        ];
        Category::query()->updateOrCreate(
            ['id' => $id],
            $data_info
        );
        return redirect()->route('category.list');
    }

    public function edit($id){
        $category = Category::where('id',$id)->first();
        return view('admin.category.edit', compact('category'));
    }

    public function delete($id){
        Category::where('id',$id)->delete();
        return redirect()->route('category.list');
    }
}
