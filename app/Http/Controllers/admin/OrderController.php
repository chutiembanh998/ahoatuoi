<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(){
        $orders = Order::with('detail')->orderBy('id','DESC')->paginate(20);
        return view('admin.order.index', compact('orders'));
    }

    public function edit($id){
        $order = Order::where('id',$id)->first();
        return view('admin.order.edit', compact('order'));
    }

    public function updateStatus(Request $request){
        $id = $request->id;
        $status = $request->status;
        Order::where('id',$id)->update('status',$status);
        return redirect()->back();
    }

    public function delete($id){
        Order::where('id',$id)->delete();
        return redirect()->back();
    }
}