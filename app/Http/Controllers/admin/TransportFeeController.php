<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\TransportFee;
use Illuminate\Http\Request;

class TransportFeeController extends Controller
{
    public function index(){
        return view('admin.transport.index');
    }

    public function load(){
        $fees = TransportFee::orderBy('id','DESC')->get();
        return view('admin.transport.load', compact('fees'))->render();
    }

    public function store(Request $request){
        if($request->ajax()){
            $data_info = [
                'district' => $request->district,
                'fee' => $request->fee,
            ];
            TransportFee::create($data_info);
        }
    }

    public function update(Request $request){
        if($request->ajax()){
            $id = $request->id;
            $district = $request->district;
            $fee = $request->fee ?? 0;

            for($i=0; $i<count($id); $i++){
                TransportFee::where('id',$id[$i])->update([
                    'district' => $district[$i],
                    'fee' => $fee[$i]
                ]);
            }
            return true;
        }
    }

    public function delete(Request $request){
        if($request->ajax()){
            $id = $request->id;
            TransportFee::where('id',$id)->delete();
        }
    }
}
