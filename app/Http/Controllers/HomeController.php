<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Category;
use App\Models\Product;
class HomeController extends Controller
{
    public function index(){
        // slide banner
        $slides = Banner::where('type',Banner::type['home_slide'])->where('show',Banner::show['show'])->orderBy('id','desc')->get();
        // side category
        $bannersCate = Banner::where('type',Banner::type['home_cate'])->where('show',Banner::show['show'])->orderBy('id','desc')->limit(3)->get();
        // home slide
        $slidesHome = Banner::where('type',Banner::type['home_banner'])->where('show',Banner::show['show'])->orderBy('id','desc')->get();
        // home banner
        $bannerHome = Banner::where('type',Banner::type['home_bannerft'])->where('show',Banner::show['show'])->first();
        // slide brand
        $slideBrand = Banner::where('type',Banner::type['slide_brand'])->where('show',Banner::show['show'])->orderBy('id','desc')->get();

        // category in tab
        $cate_home = Category::where('status',Category::status['home_tab'])
        ->orWhere('status',Category::status['home_all'])
        ->get();

        // all product hot 
        $products = Product::where('status',Product::show['show'])
        ->where('status',Product::status['hot'])
        ->orderBy('id','desc')
        ->limit(20)
        ->get();

        // category with product in home tab
        $cate_tab = Category::with(['product' => function($q){
            $q->limit(20);
        }])
        ->where('status',Category::status['home_tab'])
        ->orWhere('status',Category::status['home_all'])
        ->get();

        // category with product in home slide
        $cate_slide = Category::with(['product' => function($q){
            $q->limit(20);
        }])
        ->where('status',Category::status['home_slide'])
        ->orWhere('status',Category::status['home_all'])
        ->get();

        return view('site.home',compact('slides','cate_tab','cate_slide','cate_home','products','bannersCate','slidesHome','bannerHome','slideBrand'));
    }
}
