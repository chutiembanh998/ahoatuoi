<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Exception;

class CategoryApiController extends BaseApiController
{
    protected $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            return $this->sendSuccessData(
                $this->service->index()
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }
    public function listProduct()
    {
        try {
            return $this->sendSuccessData(
                $this->service->listProduct()
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }
    public function store(Request $request)
    {
        try {
            return $this->sendSuccessData(
                $this->service->store($request)
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }

    public function updateSeo(Request $request,$id)
    {
        try {
            return $this->sendSuccessData(
                $this->service->updateSeo($request,$id)
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }

    public function edit($id)
    {
        try {
            return $this->sendSuccessData(
                $this->service->edit($id)
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }

    public function delete($id)
    {
        try {
            return $this->sendSuccessData(
                $this->service->delete($id)
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }
}
