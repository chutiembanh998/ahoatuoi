<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\SettingService;
use Illuminate\Http\Request;
use Exception;

class SettingApiController extends BaseApiController
{
    protected $service;

    public function __construct(SettingService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            return $this->sendSuccessData(
                $this->service->index()
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }
    public function updateSeo(Request $request)
    {
        try {
            return $this->sendSuccessData(
                $this->service->updateSeo($request)
            );
        } catch (Exception $exception) {
            $this->getLogger()->error($exception);

            return $this->setMessage($exception->getMessage())->sendErrorData();
        }
    }
}
