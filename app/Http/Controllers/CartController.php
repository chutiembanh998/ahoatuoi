<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Session;
class CartController extends Controller
{
    public function cartSession(Request $request){
        if($request->ajax()){
            $qty = $request->qty ?? 1;
            $id = $request->id;
            $flag = $request->flag;
            $product = Product::where('id',$id)->selectRaw('id,title,slug,avatar,price,promt_price')->first();

            $cart = Session::get('cart-flower', []);
            $total = Session::get('total-flower', []);

            // delete product in cart
            if($flag == 'delete'){
                unset($cart[$id]);
            }
            // update + add product
            else{
                if(isset($cart[$id])) {
                    $cart[$id]['quantity'] = ($flag == 'edit') ? $qty : $cart[$id]['quantity'] + 1;
                } else {
                    $cart[$id] = [
                        "title" => $product->title,
                        "quantity" => $qty,
                        "price" => $product->price,
                        "avatar" => $product->avatar,
                        "slug" => $product->slug,
                    ];
                }
            }

            $total_price = 0;
            $total_qty = 0;
            if(!empty($cart)){
                foreach($cart as $key => $item){
                    $total_qty += $item['quantity'];
                    $total_price += ($item['quantity'] * $item['price']);
                }
            }
            $total = [
                'total_price' => $total_price,
                'total_qty' => $total_qty,
            ];
            Session::put('total-flower', $total);
            Session::put('cart-flower', $cart);

            $result = [
                'cart' => $cart,
                'total' => $total
            ];
            return response()->json($result);
        }
    }

    public function loadCartTotal(){
        $result = Session::get('total-flower', []);
        return response()->json($result);
    }

    public function tempCart(){
        return view('site.cart');
    }

    public function loadCart(){
        $carts = Session::get('cart-flower', []);
        $total = Session::get('total-flower', []);
        return view('site.load-ajax.load-carts', compact('carts','total'))->render();
    }
    
}
