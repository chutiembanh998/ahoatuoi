<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "categories";
    public $timestamps = true;

    CONST show = [
        'hide' => 0,
        'show' => 1
    ];

    CONST status = [
        'home_tab' => 1,
        'home_slide' => 2,
        'home_all' => 3
    ];

    public function product(){
        return $this->belongsToMany(Product::class, 'category_product', 'category_id', 'product_id')->where('show',Product::show['show'])->orderBy('id','desc');
    }

    public function status(){
        switch ($this->status) {
            case 1:
                echo "Tab trang chủ";
                break;
            case 2:
                echo "Line Trang chủ";
                break;
            case 3:
                echo "Tất cả";
                break; 
            default:
                echo "Không hiển thị Trang chủ";
                break;
        }
    }
}
