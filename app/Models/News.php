<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "news";
    public $timestamps = true;

    public function status(){
        switch ($this->show) {
            case 1:
                echo "Hiển thị";
                break;
            default:
                echo "Ẩn";
                break;
        }
    }
}
