<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "banners";
    public $timestamps = true;

    CONST type = [
        'home_slide' => 0,
        'home_banner' => 1,
        'home_cate' => 2,
        'home_bannerft' => 3,
        'slide_brand' => 4,
        'products_banner' => 5,
    ];

    CONST show = [
        'hide' => 0,
        'show' => 1
    ];

    public function type(){
        switch ($this->type) {
            case '1':
                echo 'Banner Trang chủ';
                break;
            case '2':
                echo 'Banner Danh mục(home)';
                break;
            case '3':
                echo 'Banner Trang chủ(foot)';
                break;
            case '4':
                echo 'Slide thương hiệu';
                break;
            case '5':
                echo 'Banner Danh mục';
                break;
            default:
                echo 'Slide Trang chủ';
                break;
        }
    }

    public function status(){
        switch ($this->show) {
            case 1:
                echo "Hiển thị";
                break;
            default:
                echo "Ẩn";
                break;
        }
    }
}
