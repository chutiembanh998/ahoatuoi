<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = "products";
    public $timestamps = true;

    CONST show = [
        'hide' => 0,
        'show' => 1
    ];
    CONST status = [
        'new' => 0,
        'hot' => 1
    ];
    
    public function category(){
        return $this->belongsToMany(Category::class, 'category_product', 'product_id', 'category_id')->where('show',Category::show['show']);
    }

    public function checkedBox($id_cat){
        $arr_cat = CategoryProduct::where('product_id',$this->id)->get();
        $arr = [];
        foreach($arr_cat as $row){
            $arr[] = $row->category_id;
        }
        if(in_array($id_cat, $arr)){
            echo "checked";
        }
    }
}
