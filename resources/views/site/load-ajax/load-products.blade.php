@if(isset($products))
<div class="grid-list-product-wrapper">
    <div class="product-grid product-view pb-20">
        <div class="row">
            @foreach($products as $item)
            <div class="product-width col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 mb-30">
                <div class="product-wrapper">
                    <div class="product-img">
                        <a href="{{ route('product', $item->slug) }}">
                            <img alt="{{ $item->title }}" src="{{ $item->avatar }}">
                        </a>
                        @if($item->promt_price != 0)
                        <span>-{{ round((100-($item->promt_price/$item->price)*100), 0) }}%</span>
                        @endif
                        <div class="product-action">
                            <a class="action-cart" title="Thêm giỏ hàng" data-id="{{ $item->id }}">
                                <i class="icon-handbag"></i>
                            </a>
                            <a class="action-compare" href="#" data-bs-toggle="modal" data-id="{{$item->id}}" title="Xem nhanh">
                                <i class="icon-magnifier-add"></i>
                            </a>
                        </div>
                    </div>
                    <div class="product-content text-center">
                        <h4>
                            <a href="{{ route('product', $item->slug) }}">{{ $item->title }}</a>
                        </h4>
                        <div class="product-price-wrapper">
                            @if($item->promt_price != 0)
                            <span>{{ number_format($item->promt_price) }}đ</span>
                            <span class="product-price-old">{{ number_format($item->price) }}đ</span>
                            @else
                            <span>{{ number_format($item->price) }}đ</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="pagination-total-pages">
        <div class="pagination-style pro-pag" data-total="{{$products->total()}}">
          {!! $products->links('vendor.pagination.custom') !!}
        </div>
    </div>
</div>
@endif