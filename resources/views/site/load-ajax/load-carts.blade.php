@if(isset($carts))
<table>
    <thead>
        <tr>
            <th>Ảnh</th>
            <th>Tên</th>
            <th>Giá tiền</th>
            <th>Số lượng</th>
            <th>Thành tiền</th>
            <th>Xóa</th>
        </tr>
    </thead>
    <tbody>
        @foreach($carts as $key => $row)
        <tr>
            <td class="product-thumbnail">
                <a href="{{ route('product',['slug' => $row['slug']]) }}"><img src="{{ $row['avatar'] }}" alt="{{ $row['title'] }}" width="100%"></a>
            </td>
            <td class="product-name"><a href="{{ route('product',['slug' => $row['slug']]) }}">{{ $row['title'] }} </a></td>
            <td class="product-price-cart"><span class="amount">{{ conversion($row['price']) }}</span></td>
            <td class="product-quantity">
       
                <div class="pro-dec-cart">
                    <input class="cart-plus-minus-box ipq" data-id="{{ $key }}" type="number" min="1" value="{{ $row['quantity'] }}">
                </div>
            </td>
            <td class="product-subtotal">{{ conversion($row['price'] * $row['quantity']) }}</td>
            <td class="product-remove">
                <a type="button" class="trash-cart" data-id="{{ $key }}"><i class="fa fa-times"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else   
Giỏ hàng của bạn rỗng
@endif