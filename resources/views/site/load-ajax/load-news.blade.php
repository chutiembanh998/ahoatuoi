@if($news->count() > 0)
@foreach($news as $row)
<div class="single-blog-wrapper mb-40">
    <div class="blog-img mb-30">
        <a href="{{ route('news.detail',['slug' => $row->slug]) }}">
            <img src="{{ $row->avatar }}" alt="">
        </a>
    </div>
    <div class="blog-content">
        <h2><a href="{{ route('news.detail',['slug' => $row->slug]) }}">{{ $row->title }}</a></h2>
        <div class="blog-date-categori">
            <ul>
                <li>{{date('d-m-Y', strtotime($row->created_at))}}</li>
                <li><a >Admin </a></li>
            </ul>
        </div>
    </div>
    <p>{{ $row->descrition }}</p>
    <div class="blog-btn-social mt-30">
        <div class="blog-btn">
            <a href="{{ route('news.detail',['slug' => $row->slug]) }}">Xem thêm</a>
        </div>
    </div>
</div>
@endforeach
<div class="pagination-total-pages mt-50">
    <div class="pagination-style pro-pag">
        {!! $news->links('vendor.pagination.custom') !!}
    </div>
</div>
@else   

@endif