<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$setting->name)
@section('canonical',$setting->website.'/bai-viet')
@section('og-url',$setting->website.'/bai-viet')
@section('og-title',$setting->title_seo)
@section('og-desc',$setting->desc_seo)
@section('og-image',$setting->logo)

@section('seo-title',$setting->title_seo)
@section('seo-desc',$setting->desc_seo)
@section('seo-keyword',$setting->key_seo)
<!-- END META SEO -->

@extends('site.layouts.master')
@section('content')
<div class="breadcrumb-area gray-bg">
            <div class="container">
                <div class="breadcrumb-content">
                    <ul>
                        <li><a href="{{ route('home') }}">Trang chủ</a></li>
                        <li class="active">Bài viết </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- blog-area start -->
        <div class="blog-area ptb-75">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9 col-md-8">
                        <div class="blog-wrapper">

                            
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-3 col-md-4">
                        <div class="blog-sidebar-wrapper sidebar-mrg">
                            <div class="blog-widget mb-50">
                                <div class="blog-search">
                                    <form class="news-form">
                                        <input id="search" type="text" placeholder="Search.....">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- blog-area end -->
        <script>
            $(document).ready(function() {
                getNews(1);
        
                function getNews(page) {
                    $('.blog-wrapper').empty();
                    var key = $('#search').val();
                    $.ajax({
                        url: " {{ route('get-news') }} ",
                        type: "POST",
                        data: {
                            page: page,
                            key: key
                        },
                        success: function(data) {
                            $('.blog-wrapper').html(data);
                        }
                    })
                }
                $('#search').keyup(function(){
                    getNews(1);
                })
                $(document).on('click','.pro-pag a', function(event) {
                    event.preventDefault();
                    let page = $(this).attr('href').split('page=')[1];
                    getProducts(page);
                    document.querySelector('.list-product-main').scrollIntoView();
                });
            })
        </script>
@endsection