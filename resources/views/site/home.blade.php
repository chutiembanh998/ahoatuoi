<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$setting->name)
@section('canonical',$setting->website)
@section('og-url',$setting->website)
@section('og-title',$setting->title_seo)
@section('og-desc',$setting->desc_seo)
@section('og-image',$setting->logo)

@section('seo-title',$setting->title_seo)
@section('seo-desc',$setting->desc_seo)
@section('seo-keyword',$setting->key_seo)
<!-- END META SEO -->


@extends('site.layouts.master')
@section('content')
<div class="slider-banner-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="row">
                    @foreach($bannersCate as $row)
                    <div class="col-lg-12">
                        <div class="single-banner img-zoom mb-30">
                            <a href="{{$row->link}}">
                                <img src="{{$row->avatar}}" alt="{{$row->title}}">
                            </a>
                            <div class="banner-content">
                                <h4>{{$row->title}}</h4>
                                <a href="{{$row->link}}">Xem ngay</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="slider-area slider-area-2">
                    <div class="slider-active owl-dot-style owl-carousel">
                        @foreach($slides as $row)
                        <div class="single-slider pt-140 pb-146 bg-img" style="background-image:url({{ $row->avatar }});">
                            <div class="slider-content slider-animated-1 pl-30">
                                <h2 class="animated">{{ $row->title }}</h2>
                                <div class="slider-btn mt-45">
                                    <a class="animated" href="{{ $row->link }}">Xem ngay</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-area pt-40 pb-70">
    <div class="container">
        <div class="product-top-bar section-border mb-35">
            <div class="section-title-wrap">
                <h3 class="section-title section-bg-white">Sản phẩm nổi bật</h3>
            </div>
            <div class="product-tab-list nav section-bg-white">
                <a class="active" data-bs-toggle="tab" href="#tab0">
                    <h4>Tất cả </h4>
                </a>
                @foreach($cate_home as $row)
                <a data-bs-toggle="tab" href="#tab{{ $row->id }}">
                    <h4>{{ $row->name }} </h4>
                </a>
                @endforeach
            </div>
        </div>
        <div class="tab-content jump">
            <div id="tab0" class="tab-pane active">
                <div class="featured-product-active owl-carousel product-nav">
                    @foreach($products as $item)
                    <div class="product-wrapper">
                        <div class="product-img">
                            <a href="{{ route('product', $item->slug) }}">
                                <img alt="{{ $item->title }}" src="{{ $item->avatar }}">
                            </a>
                            @if($item->promt_price != 0)
                            <span>-{{ round((100-($item->promt_price/$item->price)*100), 0) }}%</span>
                            @endif
                            <div class="product-action">
                                <a class="action-cart" title="Thêm giỏ hàng" data-id="{{ $item->id }}">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a class="action-compare" href="#" data-bs-toggle="modal" data-id="{{$item->id}}" title="Xem nhanh">
                                    <i class="icon-magnifier-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-content text-center">
                            <h4>
                                <a href="{{ route('product', $item->slug) }}">{{ $item->title }}</a>
                            </h4>
                            <div class="product-price-wrapper">
                                @if($item->promt_price != 0)
                                <span>{{ number_format($item->promt_price) }}đ</span>
                                <span class="product-price-old">{{ number_format($item->price) }}đ</span>
                                @else 
                                <span>{{ number_format($item->price) }}đ</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @foreach($cate_tab as $row)
            <div id="tab{{ $row->id }}" class="tab-pane">
                <div class="featured-product-active owl-carousel product-nav">
                    @foreach($row->product as $item)
                    <div class="product-wrapper">
                        <div class="product-img">
                            <a href="{{ route('product', $item->slug) }}">
                                <img alt="{{ $item->title }}" src="{{ $item->avatar }}">
                            </a>
                            @if($item->promt_price != 0)
                            <span>-{{ round((100-($item->promt_price/$item->price)*100), 0) }}%</span>
                            @endif
                            <div class="product-action">
                                <a class="action-cart" title="Thêm giỏ hàng" data-id="{{ $item->id }}">
                                    <i class="icon-handbag"></i>
                                </a>
                                <a class="action-compare" href="#" data-bs-toggle="modal" data-id="{{$item->id}}" title="Xem nhanh">
                                    <i class="icon-magnifier-add"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-content text-center">
                            <h4>
                                <a href="{{ route('product', $item->slug) }}">{{ $item->title }}</a>
                            </h4>
                            <div class="product-price-wrapper">
                                @if($item->promt_price != 0)
                                <span>{{ number_format($item->promt_price) }}đ</span>
                                <span class="product-price-old">{{ number_format($item->price) }}đ</span>
                                @else 
                                <span>{{ number_format($item->price) }}đ</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="best-selling-product">
    <div class="container">
        <div class="border-top-3 border-bottom-2 pt-70 pb-75">
            <div class="best-selling-wrap">
                <div class="best-selling-active-2 owl-carousel product-nav">
                    @foreach($slidesHome as $row)
                    <div class="single-best-selling">
                        <div class="row">
                            <div class="col-lg-6 col-xl-5 col-md-6">
                                <div class="single-best-img">
                                    <img class="tilter" src="{{ $row->avatar }}" alt="{{ $row->title }}">
                                </div>
                            </div>
                            <div class="col-lg-6 col-xl-7 col-md-6">
                                <div class="deals-content text-center deal-mrg">
                                    <img alt="{{ $row->title }}" src="{{ $row->avatar }}">
                                    <h2>Hot Deal ! Khuyến mãi giảm giá <span>20% </span></h2>
                                    <p>{{ $row->title }} </p>
                                    <div class="timer timer-style">
                                        <div data-countdown="2022/09/01"></div>
                                    </div>
                                    <div class="deals-btn">
                                        <a href="{{ $row->link }}">Xem ngay</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@foreach($cate_slide as $row)
<div class="product-area pt-70 pb-70">
    <div class="container">
        <div class="product-top-bar section-border mb-35">
            <div class="section-title-wrap">
                <h3 class="section-title section-bg-white">{{ $row->name }}</h3>
            </div>
        </div>
        <div class="featured-product-active hot-flower owl-carousel product-nav">
            @foreach( $row->product as $item )
            <div class="product-wrapper">
                <div class="product-img">
                    <a href="{{ route('product', $item->slug) }}">
                        <img alt="{{ $item->title }}" src="{{ $item->avatar }}">
                    </a>
                    @if($item->promt_price != 0)
                    <span>-{{ round((100-($item->promt_price/$item->price)*100), 0) }}%</span>
                    @endif
                    <div class="product-action">
                        <a class="action-cart" title="Thêm giỏ hàng" data-id="{{ $item->id }}">
                            <i class="icon-handbag"></i>
                        </a>
                        <a class="action-compare" href="#" data-bs-toggle="modal" data-id="{{$item->id}}" title="Xem nhanh">
                            <i class="icon-magnifier-add"></i>
                        </a>
                    </div>
                </div>
                <div class="product-content text-center">
                    <h4>
                        <a href="{{ route('product', $item->slug) }}">{{ $item->title }}</a>
                    </h4>
                    <div class="product-price-wrapper">
                        @if($item->promt_price != 0)
                        <span>{{ number_format($item->promt_price) }}đ</span>
                        <span class="product-price-old">{{ number_format($item->price) }}đ</span>
                        @else 
                        <span>{{ number_format($item->price) }}đ</span>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endforeach
<div class="brand-logo-area ">
    <div class="container">
        <div class="brand-logo-active owl-carousel border-top-3 border-bottom-2 ptb-65">
            @foreach($slideBrand as $row)
            <div class="single-brand-logo">
                <img alt="{{ $row->title }}" src="{{$row->avatar}}">
            </div>
            @endforeach
        </div>
    </div>
</div>
@if(isset($bannerHome))
<div class="new-year-offer-area pt-75 pb-75">
    <div class="container">
        <div class="new-year-offer-wrap pt-70 pb-75 bg-img" style="{{$bannerHome->avatar}}">
            <div class="new-year-offer-content text-center" >
                <h4 style="color:black">New Year Offer</h4>
                <h3 style="color:black">{{ $bannerHome->title }}</h3>
                <a href="{{ $bannerHome->link }}" style="color:black">Xem ngay</a>
            </div>
        </div>
    </div>
</div>
@endif
@endsection