<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$setting->name)
@section('canonical',$setting->website.'/checkout')
@section('og-url',$setting->website.'/checkout')
@section('og-title',$setting->title_seo)
@section('og-desc',$setting->desc_seo)
@section('og-image',$setting->logo)

@section('seo-title',$setting->title_seo)
@section('seo-desc',$setting->desc_seo)
@section('seo-keyword',$setting->key_seo)
<!-- END META SEO -->

@extends('site.layouts.master')
@section('content')
<!-- checkout-area start -->
<div class="checkout-area pb-45 pt-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="checkout-wrapper">
                    <div id="faq" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title"><span>1.</span> <a data-bs-toggle="collapse" >Thông tin khách hàng</a></h5>
                            </div>
                            <div id="payment-2" class="panel-collapse collapse show" data-bs-parent="#faq">
                                <div class="panel-body">
                                    <form id="form_checkout" method="post" action="{{ route('checkout.store') }}">
                                        @csrf
                                        <div class="billing-information-wrapper">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-info">
                                                        <label>Tên khách hàng (*)</label>
                                                        <input type="text" class="name_customer" name="name" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-info">
                                                        <label>Tên người nhận</label>
                                                        <input type="text" name="receiver_name" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-info">
                                                        <label>SĐT khách hàng (*)</label>
                                                        <input type="number" class="tel_customer" name="tel" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-info">
                                                        <label>SĐT người nhận</label>
                                                        <input type="number" name="receiver_tel" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-info">
                                                        <label>Mail nhận thông báo</label>
                                                        <input type="email" name="mail" value="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="billing-select">
                                                        <label>Quận (*)</label>
                                                        <select class="fee" name="fee">
                                                            <option value="">Chọn quận</option>
                                                            @foreach($transports as $row)
                                                            <option value="{{ $row->fee }}" >{{ $row->district }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="billing-info">
                                                        <label>Địa chỉ đầy đủ</label>
                                                        <input type="text" name="address">
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="billing-info">
                                                        <label>Ghi chú</label>
                                                        <textarea name="note" rows="8"></textarea>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="ship-wrapper">
                                                <div class="single-ship">
                                                    <input type="radio" name="pay_method" value="0" checked="">
                                                    <label>Thanh toán tiền mặt trước khi nhận hàng </label>
                                                </div>
                                                <div class="single-ship">
                                                    <input type="radio" name="pay_method" value="1">
                                                    <label>Chuyển khoản</label>
                                                </div>
                                            </div>
                                            <div class="billing-back-btn">
                                                
                                                <div class="billing-btn">
                                                    <button class="checkout-submit" type="button">Xác nhận</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="checkout-progress">
                    <h4>Đơn hàng </h4>
                    <ul>
                        <input id="total_price" type="hidden" value="{{ $total['total_price'] }}" />
                        <li>Giá: <span>{{ conversion($total['total_price']) }}</span></li>
                        <li>Ship: <span id="fee"></span></li>
                        <li>Tổng: <span id="sub_total"></span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // xóa sản phẩm giỏ hàng
    $('.fee').change(function(){
      var fee = $('.fee').val();
      var total_price = $('#total_price').val();
      var total = parseInt(total_price) + parseInt(fee ?? 0);
      $('#fee').text(new Intl.NumberFormat().format(fee ?? 0)+'đ');
      $('#sub_total').text(new Intl.NumberFormat().format(total ?? 0)+'đ');
    })
    // 
    $('.checkout-submit').click(function() {
        var name = $('.name_customer').val();
        var tel = $('.tel_customer').val();
        var fee = $('.fee').val();
        var reg_tel = /((84|0[3|5|7|8|9])+([0-9]{8})\b)/g;
        let flag = [];
        if (name == '' ) {
            $('.name_customer').css('border-bottom', '1px solid red');
            flag.push('name');
        } else {
            $('.name_customer').css('border-bottom', '1px solid #4CAF50');
        }

        if (!reg_tel.test(tel) || tel == '') {
            $('.tel_customer').css('border-bottom', '1px solid red');
            flag.push('tel');
        }else{
            $('.tel_customer').css('border-bottom', '1px solid #4CAF50');
        }

        if (fee == "" ) {
            $('.fee').css('border-bottom', '1px solid red');
            flag.push('fee');
        } else {
            $('.fee').css('border-bottom', '1px solid #4CAF50');
        }
        if(flag.length == 0){
            $('#form_checkout').submit();
        }else{
            toastr.error("Vui lòng không được bỏ trống và điền đúng SĐT  (*)")
        }
    })
    </script>
@endsection