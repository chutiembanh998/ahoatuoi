<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$product->title)
@section('canonical',route('news',['slug' => $product->slug]))
@section('og-url',route('news',['slug' => $product->slug]))
@section('og-title',$product->title)
@section('og-desc',$product->description)
@section('og-image',$product->avatar)

@section('seo-title',$product->title_seo)
@section('seo-desc',$product->desc_seo)
@section('seo-keyword',$product->key_seo)
<!-- END META SEO -->

@extends('site.layouts.master')
@section('content')
<div class="breadcrumb-area gray-bg">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="{{ route('home') }}">Trang chủ</a></li>
                <li class="active">{{ $product->title }} </li>
            </ul>
        </div>
    </div>
</div>
<div class="product-details pt-75 pb-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="product-details-img">
                    <img class="zoompro" src="{{ $product->avatar }}" data-zoom-image="{{ $product->avatar }}"
                        alt="zoom" />
                    @if($product->promt_price != 0)
                    <span>-{{ round((100-($product->promt_price/$product->price)*100), 0) }}%</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="product-details-content">
                    <h4>{{ $product->title }}</h4>
                    <div class="rating-review">
                        <div class="pro-dec-rating">
                            <i class="ion-android-star-outline theme-star"></i>
                            <i class="ion-android-star-outline theme-star"></i>
                            <i class="ion-android-star-outline theme-star"></i>
                            <i class="ion-android-star-outline theme-star"></i>
                            <i class="ion-android-star-outline"></i>
                        </div>
                        <div class="pro-dec-review">
                            <ul>
                                <li>32 Reviews </li>
                                <li> Add Your Reviews</li>
                            </ul>
                        </div>
                    </div>
                    @if($product->promt_price != 0)
                    <span>{{ number_format($product->promt_price) }}đ</span>
                    <span style="text-decoration: line-through;color: #ff3d2a; font-size:14px">{{
                        number_format($product->price) }}đ</span>
                    @else
                    <span>{{ number_format($product->price) }}đ</span>
                    @endif
                    <div class="in-stock">
                        <p>Tình trạng: <span>@if($product->stock == 1) Còn hàng @endif</span></p>
                    </div>
                    <p>{{ $product->description }} </p>
                    <div class="quality-add-to-cart">
                        <div class="quality">
                            <label>Qty:</label>
                            <input class="cart-plus-minus-box qty_number" min="1" type="number" name="qtybutton"
                                value="1">
                        </div>
                        <div class="shop-list-cart-wishlist">
                            <a class="action-cart" title="Add To Cart" data-id="{{ $product->id }}">
                                <i class="icon-handbag"></i>
                            </a>
                        </div>
                    </div>
                    <div class="pro-dec-categories">
                        <ul>
                            <li class="categories-title">Danh mục:</li>
                            @foreach($product->category as $row)
                            <li><a href="{{ route('products', $row->slug) }}">{{$row->name}},</a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="pro-dec-categories">
                        <ul>
                            <li class="categories-title">Tags: </li>
                            <li><a href="#"> Bouquet,</a></li>
                            <li><a href="#"> Event, </a></li>
                            <li><a href="#"> Gift,</a></li>
                            <li><a href="#"> Joy,</a></li>
                            <li><a href="#"> Love </a></li>
                        </ul>
                    </div>
                    <div class="pro-dec-social">
                        <ul>
                            <li><a class="tweet" href="#"><i class="ion-social-twitter"></i> Tweet</a></li>
                            <li><a class="share" href="#"><i class="ion-social-facebook"></i> Share</a></li>
                            <li><a class="google" href="#"><i class="ion-social-googleplus-outline"></i> Google+</a>
                            </li>
                            <li><a class="pinterest" href="#"><i class="ion-social-pinterest"></i> Pinterest</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="description-review-area pb-70">
    <div class="container">
        <div class="description-review-wrapper">
            <div class="description-review-topbar nav text-center">
                <a class="active" data-bs-toggle="tab" href="#des-details1">Chi tiết</a>
                <a data-bs-toggle="tab" href="#des-details2">Thẻ</a>
                <a data-bs-toggle="tab" href="#des-details3">Review</a>
            </div>
            <div class="tab-content description-review-bottom">
                <div id="des-details1" class="tab-pane active">
                    <div class="product-description-wrapper">
                        {{ $product->content }}
                    </div>
                </div>
                <div id="des-details2" class="tab-pane">
                    <div class="product-anotherinfo-wrapper">
                        <ul>
                            <li><span>Tags:</span></li>
                            <li><a href="#"> bouquet,</a></li>
                            <li><a href="#"> event,</a></li>
                            <li><a href="#"> gift,</a></li>
                            <li><a href="#"> joy,</a></li>
                            <li><a href="#"> love ,</a></li>
                            <li><a href="#"> special</a></li>
                        </ul>
                    </div>
                </div>
                <div id="des-details3" class="tab-pane">
                    <div class="rattings-wrapper">
                        <div class="sin-rattings">
                            <div class="star-author-all">
                                <div class="ratting-star f-left">
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <span>(5)</span>
                                </div>
                                <div class="ratting-author f-right">
                                    <h3>tayeb rayed</h3>
                                    <span>12:24</span>
                                    <span>9 March 2018</span>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor
                                sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Utenim ad minim veniam, quis nost.</p>
                        </div>
                        <div class="sin-rattings">
                            <div class="star-author-all">
                                <div class="ratting-star f-left">
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <i class="ion-star theme-color"></i>
                                    <span>(5)</span>
                                </div>
                                <div class="ratting-author f-right">
                                    <h3>farhana shuvo</h3>
                                    <span>12:24</span>
                                    <span>9 March 2018</span>
                                </div>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Utenim ad minim veniam, quis nost rud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor
                                sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore magna aliqua. Utenim ad minim veniam, quis nost.</p>
                        </div>
                    </div>
                    <div class="ratting-form-wrapper">
                        <h3>Add your Comments :</h3>
                        <div class="ratting-form">
                            <form action="#">
                                <div class="star-box">
                                    <h2>Rating:</h2>
                                    <div class="ratting-star">
                                        <i class="ion-star theme-color"></i>
                                        <i class="ion-star theme-color"></i>
                                        <i class="ion-star theme-color"></i>
                                        <i class="ion-star theme-color"></i>
                                        <i class="ion-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="rating-form-style mb-20">
                                            <input placeholder="Name" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="rating-form-style mb-20">
                                            <input placeholder="Email" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="rating-form-style form-submit">
                                            <textarea name="message" placeholder="Message"></textarea>
                                            <input type="submit" value="add review">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-area pb-70">
    <div class="container">
        <div class="product-top-bar section-border mb-35">
            <div class="section-title-wrap">
                <h3 class="section-title section-bg-white">Sản phẩm liên quan</h3>
            </div>
        </div>
        <div class="featured-product-active hot-flower owl-carousel product-nav">
            @foreach($rela_products as $item)
            <div class="product-wrapper">
                <div class="product-img">
                    <a href="{{ route('product', $item->slug) }}">
                        <img alt="{{ $item->title }}" src="{{ $item->avatar }}" width="100%">
                    </a>
                    @if($item->promt_price != 0)
                    <span>-{{ round((100-($item->promt_price/$item->price)*100), 0) }}%</span>
                    @endif
                    <div class="product-action">
                        <a class="action-wishlist" href="#" title="Wishlist">
                            <i class="icon-heart"></i>
                        </a>
                        <a class="action-cart" title="Add To Cart" data-id="{{ $item->id }}">
                            <i class="icon-handbag"></i>
                        </a>
                        <a class="action-compare" href="#" data-bs-toggle="modal" data-id="{{$item->id}}" title="Xem nhanh">
                            <i class="icon-magnifier-add"></i>
                        </a>
                    </div>
                </div>
                <div class="product-content text-center">
                    <h4>
                        <a href="{{ route('product', $item->slug) }}">{{ $item->title }}</a>
                    </h4>
                    <div class="product-price-wrapper">
                        @if($item->promt_price != 0)
                        <span>{{ number_format($item->promt_price) }}đ</span>
                        <span class="product-price-old">{{ number_format($item->price) }}đ</span>
                        @else
                        <span>{{ number_format($item->price) }}đ</span>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
</div>
@endsection