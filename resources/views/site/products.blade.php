<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$category->name)
@section('canonical',route('products',['slug' => $category->slug]))
@section('og-url',route('products',['slug' => $category->slug]))
@section('og-title',$category->title_seo)
@section('og-desc',$category->desc_seo)
@section('og-image',$category->avatar)

@section('seo-title',$category->title_seo)
@section('seo-desc',$category->desc_seo)
@section('seo-keyword',$category->key_seo)
<!-- END META SEO -->

@extends('site.layouts.master')
@section('content')
<div class="breadcrumb-area gray-bg">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="{{ route('home') }}">Trang chủ</a></li>
                <li class="active">{{$category->name}} </li>
                <input type="hidden" value="{{$category->id}}" id="category_id">
            </ul>
        </div>
    </div>
</div>
<div class="shop-page-area pt-75 pb-75">
    <div class="container">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">
                <div class="banner-area pb-30">
                    <a href="{{$banner->link ?? null}}"><img alt="{{$banner->title ?? null}}" src="{{$banner->avatar ?? null}}"></a>
                </div>
                <div class="shop-topbar-wrapper">
                    <div class="shop-topbar-left">
                        <p>Hiển thị <span class="start-limit">0</span> - <span class="end-limit">0</span> trên <span class="total_pro">0</span> sản phẩm </p>
                    </div>
                    <div class="product-sorting-wrapper">
                        <div class="product-shorting shorting-style">
                            <label>Hiển thị:</label>
                            <select id="limit">
                                <option value="9"> 9</option>
                                <option value="15"> 15</option>
                                <option value="24"> 24</option>
                            </select>
                        </div>
                        <div class="product-show shorting-style">
                            <label>Sắp xếp theo:</label>
                            <select id="sort">
                                <option value="new">Mới nhất</option>
                                <option value="price-up">Giá tăng dần</option>
                                <option value="price-down"> Giá giảm dần</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="pro-list">
                    @include('site.load-ajax.load-products')
                </div>
            </div>
            <div class="col-lg-3">
                <div class="shop-sidebar-wrapper gray-bg-7 shop-sidebar-mrg">
                    <div class="shop-widget mt-40 shop-sidebar-border pt-35 mb-20">
                        <h4 class="shop-sidebar-title">Tìm kiếm</h4>
                        <div class="shop-tags mt-25">
                            <input type="text" class="search" id="search" placeholder="Tìm kiếm...">
                        </div>
                    </div>
                    <div class="shop-widget">
                        <h4 class="shop-sidebar-title">Danh mục sản phẩm</h4>
                        <div class="shop-catigory">
                            <ul id="faq">
                                @foreach($categories as $row)
                                <li> <a href="{{ route('products',$row->slug) }}">{{ $row->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="shop-widget mt-40 shop-sidebar-border pt-35">
                        <h4 class="shop-sidebar-title">Popular Tags</h4>
                        <div class="shop-tags mt-25">
                            <ul>
                                <li><a href="#">Bouquet</a></li>
                                <li><a href="#">Event</a></li>
                                <li><a href="#">Gift</a></li>
                                <li><a href="#">Joy</a></li>
                                <li><a href="#">Love </a></li>
                                <li><a href="#">Special</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        getProducts(1);

        function getProducts(page) {
            $('.pro-list').empty();
            var id = $('#category_id').val();
            var limit = $('#limit').val();
            var sort = $('#sort').val();
            var search = $('#search').val();
            $.ajax({
                url: " {{ route('get-products') }} ",
                type: "POST",
                data: {
                    page: page,
                    id: id,
                    limit: limit,
                    sort: sort,
                    search: search
                },
                success: function(data) {
                    $('.pro-list').html(data);
                    let total = $('.pro-pag').attr('data-total');
                    $('.total_pro').text(total);
                    let start = limit*(page-1) + 1;
                    $('.start-limit').text(start);
                    let end = limit*page;
                    $('.end-limit').text(end);
                }
            })
        }

        $('#limit').change(function(){
            getProducts(1);
        })
        $('#sort').change(function(){
            getProducts(1);
        })
        $('#search').keyup(function(){
            getProducts(1);
        })
        $(document).on('click','.pro-pag a', function(event) {
            event.preventDefault();
            let page = $(this).attr('href').split('page=')[1];
            getProducts(page);
            document.querySelector('.list-product-main').scrollIntoView();
        });
    })
</script>
@endsection