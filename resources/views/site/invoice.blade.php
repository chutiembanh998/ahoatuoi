@extends('site.layouts.master')
@section('content')
<!-- checkout-area start -->
<div class="checkout-area pb-45 pt-65">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="checkout-wrapper">
                    <div id="faq" class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h5 class="panel-title"><span></span> <a data-bs-toggle="collapse"
                                        >Đơn hàng của bạn</a></h5>
                            </div>
                            <div id="payment-6" class="panel-collapse collapse show" data-bs-parent="#faq">
                                <div class="panel-body">
                                    <div class="order-review-wrapper">
                                        <div class="order-review">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th class="width-1">Tên sản phẩm</th>
                                                            <th class="width-2">Giá</th>
                                                            <th class="width-3">SL</th>
                                                            <th class="width-4">Thành tiền</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($bill->detail as $row)
                                                        <tr>
                                                            <td>
                                                                <div class="o-pro-dec">
                                                                    <p>{{ $row->title }}</p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="o-pro-price">
                                                                    <p>{{ conversion($row->price) }}</p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="o-pro-qty">
                                                                    <p>{{ $row->quantity }}</p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="o-pro-subtotal">
                                                                    <p>{{ conversion($row->price * $row->quantity) }}</p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td colspan="3">Thành tiền </td>
                                                            <td colspan="1">{{ conversion($bill->total_price) }}</td>
                                                        </tr>
                                                        <tr class="tr-f">
                                                            <td colspan="3">Phí vận chuyển</td>
                                                            <td colspan="1">{{ conversion($bill->fee) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3">Tổng tiền</td>
                                                            <td colspan="1">{{ conversion($bill->total_price + $bill->fee) }}</td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="billing-back-btn">
                                                <div class="billing-btn">
                                                    <button><a href="{{ route('home') }}" >Về trang chủ</a></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="checkout-progress">
                    <h4>Thông tin khách hàng</h4>
                    <ul>
                        <li>Tên KH: <span>{{ $bill->name }} - {{ $bill->tel }}</span></li>
                        <li>Tên người nhận: <span>{{ $bill->receiver_name }} - {{ $bill->receiver_tel }}</span></li>
                        <li>Địa chỉ: <span>{{ $bill->address }}</span></li>
                        <li>Phương thức thanh toán: <span>{{ ($bill->pay_method == 0) ? 'Thanh toán tiền mặt' : 'Chuyển khoản' }}</span></li>
                        <li>Ghi chú:<span>{{ $bill->note }}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection