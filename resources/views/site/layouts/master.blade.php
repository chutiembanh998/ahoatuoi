<!doctype html>
<html class="no-js" lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Hoa Tươi</title>
        <!-- META SEO -->
        @include('includes.meta_seo')
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('site/assets/img/favicon.png') }}">
		<meta name="csrf-token" content="{{ csrf_token() }}">
        <base href="{{asset('')}}">
		<!-- all css here -->
        <link rel="stylesheet" href="{{ asset('site/assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/meanmenu.min.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('site/assets/css/responsive.css') }}">
        <script src="{{ asset('site/assets/js/vendor/modernizr-3.11.7.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
        <!-- toast -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    </head>
    <body>
        <!-- header start -->
        @include('site.layouts.header')
        @yield('content')
        @include('site.layouts.footer')	
		<!-- all js here -->
        <script src="{{ asset('site/assets/js/vendor/jquery-v2.2.4.min.js') }}"></script>
        <script src="{{ asset('site/assets/js/popper.js') }}"></script>
        <script src="{{ asset('site/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('site/assets/js/imagesloaded.pkgd.min.js') }}"></script>
        <script src="{{ asset('site/assets/js/isotope.pkgd.min.js') }}"></script>
        <script src="{{ asset('site/assets/js/ajax-mail.js') }}"></script>
        <script src="{{ asset('site/assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('site/assets/js/plugins.js') }}"></script>
        <script src="{{ asset('site/assets/js/main.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
    </body>
</html>
