<header class="header-area clearfix">
    <div class="header-bottom transparent-bar">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-6">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img width="100%" alt="{{ $setting->name }}" src="{{ $setting->logo }}">
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-9 col-6">
                    <div class="header-bottom-right">
                        <div class="main-menu">
                            <nav>
                                <ul>
                                    @foreach($categories_header as $row)
                                        <li class="top-hover"><a href="{{ route('products', $row->slug) }}">{{ $row->name }} </a></li>
                                    @endforeach
                                    <li><a href="{{ route('news') }}">Bài viết</a></li>
                                    <li><a href="contact.html">Liên hệ</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="header-cart">
                            <a href="{{ route('cart') }}">
                                <div class="cart-icon">
                                    <i class="ion-bag"></i>
                                    <span class="count-style total_qty">0</span>
                                </div>
                                <div class="cart-text">
                                    <span class="digit">Giỏ hàng</span>
                                    <span class="total_price">0đ</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu-area">
                <div class="mobile-menu">
                    <nav id="mobile-menu-active">
                        <ul class="menu-overflow">
                            <li><a href="{{ route('home') }}">Trang chủ</a>
                            </li>
                            @foreach($categories_header as $row)
                            <li><a href="{{ route('products', $row->slug) }}">{{ $row->name }} </a></li>
                            @endforeach
                            <li><a href="{{ route('news') }}">Bài viết</a></li>
                            <li><a href="contact.html"> Contact us </a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
