<footer class="footer-area pt-58 gray-bg-3">
    <div class="footer-top gray-bg-3 pb-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer-widget footer-widget-red footer-black-color mb-40">
                        <div class="footer-title mb-30">
                            <h4>Về chúng tôi</h4>
                        </div>
                        <div class="footer-about">
                            <p>{{ $setting->slogan }}</p>
                            <div class="footer-contact mt-20">
                                <ul>
                                    <li>Địa chỉ: {{ $setting->address }}</li>
                                    <li>Số điện thoại: {{ $setting->tel }} </li>
                                    <li>Email: <a href="#">{{ $setting->mail }}</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="social-icon">
                            <ul>
                                <li><a class="facebook" href="#"><i class="ion-social-facebook"></i></a></li>
                                <li><a class="twitter" href="#"><i class="ion-social-twitter"></i></a></li>
                                <li><a class="instagram" href="#"><i class="ion-social-instagram-outline"></i></a></li>
                                <li><a class="googleplus" href="#"><i class="ion-social-googleplus-outline"></i></a></li>
                                <li><a class="rss" href="#"><i class="ion-social-rss"></i></a></li>
                                <li><a class="dribbble" href="#"><i class="ion-social-dribbble-outline"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title mb-30">
                            <h4>Information</h4>
                        </div>
                        <div class="footer-content">
                            <ul>
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="#">Delivery Information</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                                <li><a href="#">Customer Service</a></li>
                                <li><a href="#">Return Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title mb-30">
                            <h4>Danh mục</h4>
                        </div>
                        <div class="footer-content">
                            <ul>
                                @foreach($categories_header as $row)
                                <li><a href="{{ route('products', $row->slug) }}">{{ $row->name }} </a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title mb-30">
                            <h4>Join Our Newsletter Now</h4>
                        </div>
                        <div class="footer-newsletter">
                            <p>Get E-mail updates about our latest shop and special offers.</p>
                                <div id="mc_embed_signup" class="subscribe-form">
                                <form action="https://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll" class="mc-form">
                                        <input type="email" value="" name="EMAIL" class="email" placeholder="Your Email Address..." required>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div class="mc-news" aria-hidden="true"><input type="text" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef" tabindex="-1" value=""></div>
                                        <div class="clear-2"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom pb-25 pt-25 gray-bg-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="copyright">
                        <p>Copyright © <a href="#">Phuler</a>. All Right Reserved.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="payment-img f-right">
                        <a href="#">
                            <img alt="" src="assets/img/icon-img/payment.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Modal -->
@include('includes.modal_product')
<!-- Modal end --> 
<script>
$(document).ready(function(){
    loadCart();
    // Load giỏ hàng
    function loadCart(){
        $.ajax({
            url: "{{ route('cart.load-total') }}",
            type: "GET",
            success:function(data){
                $('.total_qty').text(data.total_qty);
                $('.total_price').text(new Intl.NumberFormat().format(data.total_price ?? 0)+'đ');
            }
        })
    }
    // show modal product detail 
    $(document).on('click','.action-compare',function(){
        var id = $(this).attr('data-id');
        $.ajax({
            url: "{{ route('product-modal') }}",
            type: "POST",
            data: {id:id},
            success:function(data){
                $('#pro-img').html('<img width="100%" src="'+data.avatar+'" alt="'+data.title+'">');
                $('#title_prm').text(data.title);
                if(data.promt_price != 0){
                    $('#price_prm').html('<span class="product-price-old">'+data.price.toLocaleString('en-US')+'đ </span><span>'+data.promt_price.toLocaleString('en-US')+'đ </span>');
                }else{
                    $('#price_prm').html('<span>'+data.price.toLocaleString('en-US')+'đ </span>');
                }
                $('#desc_prm').text(data.description);
                $('.btn-add').html('<button class="action-cart" data-id="'+id+'">Thêm giỏ hàng</button>');
                $('#productModal').modal('show');
            }
        })
    })
    // Thêm giỏ hàng
    $(document).on('click','.action-cart',function(){
        var id = $(this).attr('data-id');
        var qty = $(".qty_number").val() ?? 1;
        $.ajax({
            url: "{{ route('cart.session') }}",
            type: "POST",
            data: {id:id, qty:qty},
            success:function(data){
                toastr.success('Đã thêm giỏ hàng');
                location.href = 'cart.html';
                loadCart();
            }
        })
    })
    $(document).on('change','.ipq',function(){
        loadCart();

    })
    $(document).on('click','.trash-cart',function(){
        loadCart();
    })  
    
})
</script>