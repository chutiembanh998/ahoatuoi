<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$setting->name)
@section('canonical',$setting->website.'/cart')
@section('og-url',$setting->website.'/cart')
@section('og-title',$setting->title_seo)
@section('og-desc',$setting->desc_seo)
@section('og-image',$setting->logo)

@section('seo-title',$setting->title_seo)
@section('seo-desc',$setting->desc_seo)
@section('seo-keyword',$setting->key_seo)
<!-- END META SEO -->
@extends('site.layouts.master')
@section('content')
<div class="breadcrumb-area gray-bg">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="{{ route('home') }}">Trang chủ</a></li>
                <li class="active">Giỏ hàng </li>
            </ul>
        </div>
    </div>
</div>
    <!-- shopping-cart-area start -->
<div class="cart-main-area pt-60 pb-65">
    <div class="container">
        <h3 class="page-title">Giỏ hàng của bạn</h3>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="table-content table-responsive" id="load-cart">
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cart-shiping-update-wrapper">
                            <div class="cart-shiping-update">
                                <a href="{{ route('home') }}">Mua thêm</a>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="grand-totall">
                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gary-cart">Tổng giỏ hàng</h4>
                                    </div>
                                    <h4 class="grand-totall-title">Tổng giá  <span class="total_price"> 0đ</span></h4>
                                    <a href="{{ route('checkout') }}">Thanh toán</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    {{-- <div class="col-lg-4 col-md-6">
                        <div class="cart-tax">
                            <div class="title-wrap">
                                <h4 class="cart-bottom-title section-bg-gray">Estimate Shipping And Tax</h4>
                            </div>
                            <div class="tax-wrapper">
                                <p>Enter your destination to get a shipping estimate.</p>
                                <div class="tax-select-wrapper">
                                    <div class="tax-select">
                                        <label>
                                            * Country
                                        </label>
                                        <select class="email s-email s-wid">
                                            <option>Bangladesh</option>
                                            <option>Albania</option>
                                            <option>Åland Islands</option>
                                            <option>Afghanistan</option>
                                            <option>Belgium</option>
                                        </select>
                                    </div>
                                    <div class="tax-select">
                                        <label>
                                            * Region / State
                                        </label>
                                        <select class="email s-email s-wid">
                                            <option>Bangladesh</option>
                                            <option>Albania</option>
                                            <option>Åland Islands</option>
                                            <option>Afghanistan</option>
                                            <option>Belgium</option>
                                        </select>
                                    </div>
                                    <div class="tax-select">
                                        <label>
                                            * Zip/Postal Code
                                        </label>
                                        <input type="text">
                                    </div>
                                    <button class="cart-btn-2" type="submit">Get A Quote</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="discount-code-wrapper">
                            <div class="title-wrap">
                                <h4 class="cart-bottom-title section-bg-gray">Use Coupon Code</h4> 
                            </div>
                            <div class="discount-code">
                                <p>Enter your coupon code if you have one.</p>
                                <form>
                                    <input type="text" required="" name="name">
                                    <button class="cart-btn-2" type="submit">Apply Coupon</button>
                                </form>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<script>

getList();
function getList(){
    $.ajax({
        url:"{{ route('cart.load') }}",
        method:'POST',
        data:{
            "_token": "{{ csrf_token() }}",
        },
        success:function(data){
          $('#load-cart').html(data);
        }
    })
}

// update giỏ hàng
$(document).on('change','.ipq',function(){
    var id = $(this).attr('data-id');
    var qty = $(this).val();
    $(this).prop('disabled', true);
    $.ajax({
        type: "POST",
        url: "{{ route('cart.session') }}",
        data: {
            "_token": "{{ csrf_token() }}",
            id: id,
            qty: qty,
            flag: 'edit'
        },
        success: function(){
            getList();
            toastr.success("Cập nhật giỏ hàng thành công")
        }
    })
})

// xóa sản phẩm giỏ hàng
$(document).on('click','.trash-cart',function(){
  var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        url: "{{ route('cart.session') }}",
        data: {
            "_token": "{{ csrf_token() }}",
            id: id,
            flag: 'delete'
        },
        success: function(){
            getList();
            toastr.success("Cập nhật giỏ hàng thành công")
        }
    })
})
// Xóa
</script>
@endsection