<!-- META SEO -->
@extends('includes.meta_seo')

@section('title',$news->title)
@section('canonical',route('news',['slug' => $news->slug]))
@section('og-url',route('news',['slug' => $news->slug]))
@section('og-title',$news->title)
@section('og-desc',$news->description)
@section('og-image',$news->avatar)

@section('seo-title',$news->title_seo)
@section('seo-desc',$news->desc_seo)
@section('seo-keyword',$news->key_seo)
<!-- END META SEO -->

@extends('site.layouts.master')
@section('content')
<!-- header end -->
<div class="breadcrumb-area gray-bg">
    <div class="container">
        <div class="breadcrumb-content">
            <ul>
                <li><a href="{{ route('home') }}">Trang chủ</a></li>
                <li class="active">{{ $news->title }} </li>
            </ul>
        </div>
    </div>
</div>
<!-- blog-area start -->
<div class="blog-area ptb-75">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12">
                <div class="blog-details-wrapper">
                    <div class="single-blog-wrapper">
                        <div class="quote-post mb-55">
                            <div class="quote-content">
                                <span>{{date('d-m-Y', strtotime($news->created_at))}}</span>
                                <h3>{{ $news->title }}</h3>
                            </div>
                        </div>
                        <p>{!! $news->content !!}</p>
                       
                    </div>
                </div>
                <div class="blog-sidebar-wrapper sidebar-mrg">
                    <div class="blog-widget mb-45">
                        <h4 class="blog-widget-title mb-25">Bài viết liên quan</h4>
                        <div class="blog-recent-post">
                            @foreach($reles as $row)
                            <div class="recent-post-wrapper mb-25">
                                <div class="recent-post-img">
                                    <a href="#"><img src="{{ $row->avatar }}" alt="{{ $row->title }}"></a>
                                </div>
                                <div class="recent-post-content">
                                    <h4><a href="{{ route('news.detail',['slug' => $row->slug]) }}">{{ $row->title }}</a></h4>
                                    <span>{{date('d-m-Y', strtotime($row->created_at))}}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog-area end -->
@endsection