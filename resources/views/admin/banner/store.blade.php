@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Thêm Banner </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('banner.list') }}">Danh sách</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                </ol>
            </nav>
        </div>
        
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Thêm mới</h4>
                            <p class="card-description"> Thông tin chi tiết </p>
                            <form class="forms-sample" action="{{ route('banner.store') }}" method="POST" enctype="multipart/form-data" id="myForm">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleSelectGender">Hiển thị ở</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="0">Slide Trang chủ</option>
                                        <option value="1">Banner Trang chủ</option>
                                        <option value="2">Banner Danh mục(home)</option>
                                        <option value="3">Banner Trang chủ(foot)</option>
                                        <option value="4">Slide thương hiệu</option>
                                        <option value="5">Banner Danh mục</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="title">Tiêu đề</label>
                                    <input type="text" name="title" class="form-control" id="title">
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="0"> Ẩn <i class="input-helper"></i></label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="1" checked> Hiển thị <i class="input-helper"></i></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title">Link</label>
                                    <input type="text" name="link" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>File upload</label>
                                    <input type="file" name="avatar" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled=""
                                            placeholder="Upload Image">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-gradient-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-gradient-primary me-2" id="btn-product">Cập nhật</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    // $('#btn-product').click(function(){
    //     $('#myForm').submit();
    // })
</script>
@endsection