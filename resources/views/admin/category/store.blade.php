@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Thêm danh mục </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('category.list') }}">Danh sách</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                </ol>
            </nav>
        </div>
        <form class="forms-sample" action="{{ route('category.store') }}" method="POST" enctype="multipart/form-data" id="myForm">
            @csrf
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Thêm mới</h4>
                            <p class="card-description"> Thông tin chi tiết </p>
                                <div class="form-group">
                                    <label for="title">Tên danh mục</label>
                                    <input type="text" name="name" class="form-control" id="name">
                                    <span class="mess-error" id="name_error"></span>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="1"> Hiển thị ở Tab Trang chủ <i class="input-helper"></i></label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="2"> Hiển thị ở Trang chủ <i class="input-helper"></i></label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="3"> Hiển thị tất cả <i class="input-helper"></i></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>File upload</label>
                                    <input type="file" name="avatar" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled=""
                                            placeholder="Upload Image">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-gradient-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-description"> Thông tin SEO </p>
                            <div class="form-group">
                                <label for="title_seo">Tiêu đề</label>
                                <input type="text" name="title_seo" class="form-control" id="title_seo">
                            </div>
                            <div class="form-group">
                                <label for="desc_seo">Mô tả</label>
                                <textarea  name="desc_seo" class="form-control desc_seo" id="desc_seo" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="key_seo">Từ khóa</label>
                                <textarea  name="key_seo" class="form-control key_seo" id="key_seo" rows="6"></textarea>
                            </div>
                            <button type="button" class="btn btn-gradient-primary me-2" id="btn-product">Cập nhật</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('#btn-product').click(function(){
        let error = [];
        var name = $('#name').val();
        if(name == ''){
            $('#name_error').text("Tên không để trống");
            toastr.error("Tên không để trống");
            error.push('name');
        }else{
            $('#name_error').text('');
        }

        if (error.length == 0) {
            $('#myForm').submit();
        }else{
            toastr.error("Xin vui lòng điền đủ thông tin")
        }
    })
</script>
@endsection