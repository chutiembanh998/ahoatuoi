@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Quản lý sản phẩm </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Quản lý danh mục</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Danh sách</h4>
                        <a href="{{ route('category.temp-store') }}" style="margin:10px 0px;" class="btn btn-info btn-fw">Thêm mới</a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tên</th>
                                    <th>Hình ảnh</th>
                                    <th>Hiển thị ở</th>
                                    <th>Hoạt động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $key => $row)
                                <tr>
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $row->name }}<br>( {{ $row->product_count }} sản phẩm )</td>
                                    <td><img src="{{ $row->avatar }}"></td>
                                    <td>{{ $row->status() }}</td>
                                    <td style="font-size: 20px">
                                        <a href="{{ route('category.edit',['id' => $row->id]) }}"><i class="mdi mdi-border-color"></i></a>
                                        <a href="{{ route('category.delete',['id' => $row->id]) }}"><i class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $categories->links('vendor.pagination.admin') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection