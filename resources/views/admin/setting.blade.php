@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Cập nhật thông tin trang web </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Cập nhật</li>
                </ol>
            </nav>
        </div>
        <form class="forms-sample" action="{{ route('setting.store') }}" method="POST" enctype="multipart/form-data" id="myForm">
            @csrf
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                          <h4 class="card-title">Thông tin trang web</h4>
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Tên website(*)</label>
                                  <div class="col-sm-9">
                                    <input id="name" name="name" type="text" class="form-control" value="{{ $setting->name }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Số điện thoại(*)</label>
                                  <div class="col-sm-9">
                                    <input id="tel" name="tel" type="number" class="form-control" value="{{ $setting->tel }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Mail</label>
                                  <div class="col-sm-9">
                                    <input name="mail" type="text" class="form-control" value="{{ $setting->mail }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Địa chỉ</label>
                                  <div class="col-sm-9">
                                    <input name="address" type="text" class="form-control" value="{{ $setting->address }}">
                                  </div>
                                </div>
                              </div>
                              
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Website</label>
                                  <div class="col-sm-9">
                                    <input name="website" type="text" class="form-control" value="{{ $setting->website }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Fanpage</label>
                                  <div class="col-sm-9">
                                    <input name="fanpage" type="text" class="form-control" value="{{ $setting->fanpage }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Favicon</label>
                                  <input type="file" name="favicon" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" value="{{ $setting->favicon }}" class="form-control file-upload-info" disabled=""
                                            placeholder="Upload Image">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-gradient-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Logo</label>
                                  <input type="file" name="logo" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" value="{{ $setting->logo }}" class="form-control file-upload-info" disabled=""
                                            placeholder="Upload Image">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-gradient-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Phần trăm giá</label>
                                  <div class="col-sm-9">
                                    <input name="percent_price" type="text" class="form-control" value="{{ $setting->percent_price }}">
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Tiêu đề SEO</label>
                                  <div class="col-sm-9">
                                    <input name="title_seo" type="text" class="form-control" value="{{ $setting->title_seo }}">
                                  </div>
                                </div>
                              </div>                             
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Mô tả SEO</label>
                                  <div class="col-sm-9">
                                    <textarea name="desc_seo" rows="8" class="form-control">{{ $setting->desc_seo }}</textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Từ khóa SEO</label>
                                  <div class="col-sm-9">
                                    <textarea name="key_seo" rows="8" class="form-control">{{ $setting->key_seo }}</textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Code Analytics</label>
                                  <div class="col-sm-9">
                                    <textarea name="analytics" rows="8" class="form-control">{{ $setting->analytics }}</textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Slogan</label>
                                  <div class="col-sm-9">
                                    <textarea name="slogan" rows="8" class="form-control">{{ $setting->slogan }}</textarea>
                                  </div>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group row">
                                  <label class="col-sm-3 col-form-label">Map</label>
                                  <div class="col-sm-9">
                                    <textarea name="map" rows="8" class="form-control">{{ $setting->map }}</textarea>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <button type="button" class="btn btn-gradient-primary me-2" id="btn-setting">Cập nhật</button>
                        </div>
                      </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $('#btn-setting').click(function(){
        let error = [];
        var name = $('#name').val();
        var tel = $('#tel').val();

        if(name == ''){
            $('#title_error').text("Tiêu đề không để trống");
            toastr.error("Tiêu đề không để trống")
            error.push('name');
        }else{
            $('#title_error').text('');
        }

        if(tel == ''){
            $('#price_error').text("Giá không để trống");
            toastr.error("Giá không để trống")
            error.push('tel');
        }else{
            $('#price_error').text('');
        }


        if (error.length == 0) {
            $('#myForm').submit();
        }else{
            toastr.error("Xin vui lòng điền đủ thông tin")
        }
    })
</script>
@endsection