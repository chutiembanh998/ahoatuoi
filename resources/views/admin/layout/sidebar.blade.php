<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-profile">
        <a href="#" class="nav-link">
          <div class="nav-profile-image">
            <img src="{{ Auth::user()->avatar }}" alt="profile">
            <span class="login-status online"></span>
            <!--change to offline or busy as needed-->
          </div>
          <div class="nav-profile-text d-flex flex-column">
            <span class="font-weight-bold mb-2">{{ Auth::user()->name }}</span>
            <span class="text-secondary text-small">Project Manager</span>
          </div>
          <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <span class="menu-title">Dashboard</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="">
          <span class="menu-title">Kiểm tra từ khóa</span>
          <i class="mdi mdi-contacts menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <span class="menu-title">Quản lý sản phẩm</span>
          <i class="menu-arrow"></i>
          <i class="mdi mdi-package menu-icon"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('category.list') }}">Danh mục</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('product.list') }}">Sản phẩm</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('order.list') }}">Hóa đơn</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('transport.list') }}">Phí vận chuyển</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#ui-blog" aria-expanded="false" aria-controls="ui-blog">
          <span class="menu-title">Quản lý bài viết</span>
          <i class="menu-arrow"></i>
          <i class="mdi mdi-package menu-icon"></i>
        </a>
        <div class="collapse" id="ui-blog">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('news.list') }}">Bài viết</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('banner.list') }}">
          <span class="menu-title">Quản lý banner</span>
          <i class="mdi mdi-contacts menu-icon"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('setting.list') }}">
          <span class="menu-title">Quản lý Website</span>
          <i class="mdi mdi-contacts menu-icon"></i>
        </a>
      </li>
    </ul>
  </nav>