@if($fees->count() > 0)
<table class="table table-hover">
    <thead>
        <tr>
            <th>Quận huyện</th>
            <th>Phí vận chuyển</th>
            <th>Xóa</th>
        </tr>
    </thead>
    <tbody>
        @foreach($fees as $row)
        <tr>
            <input class="id" name="id" type="hidden" value="{{ $row->id }}">
            <td><input type="text" name="district" class="form-control district" value="{{ $row->district }}"></td>
            <td><input type="number" name="fee" class="form-control fee" value="{{ $row->fee }}"></td>
            <td style="font-size: 24px">
                <button type="button" class="btn btn-inverse-danger btn-icon btn-delete" data-id="{{ $row->id }}">
                    <i class="mdi mdi-delete"></i>
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif