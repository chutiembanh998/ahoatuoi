<!-- Trigger/Open The Modal -->
<style>
    .modal-wrapper {
        width: 100%;
        height: 100%;
        position: fixed;
        top: 0;
        left: 0;
        background: rgba(41, 171, 164, 0.8);
        visibility: hidden;
        opacity: 0;
        transition: all 0.25s ease-in-out;
    }

    .modal-wrapper.open {
        opacity: 1;
        visibility: visible;
    }

    .modal {
        width: 600px;
        height: 350px;
        display: block;
        margin: 50% 0 0 -300px;
        position: relative;
        top: 50%;
        left: 50%;
        background: #fff;
        opacity: 0;
        transition: all 0.5s ease-in-out;
    }

    .modal-wrapper.open .modal {
        margin-top: -200px;
        opacity: 1;
    }

    .head {
        width: 100%;
        height: 60px;
        padding: 12px 30px;
        overflow: hidden;
        background: #e2525c;
    }

    .btn-close {
        font-size: 28px;
        display: block;
        float: right;
        color: #fff;
    }

    .content {
        padding: 25px;
    }
</style>
<div class="modal-wrapper">
    <div class="modal">
        <div class="head">

            <a class="btn-close trigger">
                <i class="fa fa-times" aria-hidden="true"></i>
            </a>
        </div>
        <div class="content">
            <h4 class="card-title" style="margin:20px 0px">Thêm mới</h4>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="title_seo">Quận/Huyện</label>
                    <input type="text" name="title_seo" class="form-control" id="district">
                </div>
                <div class="form-group col-md-6">
                    <label for="title_seo">Phí ship</label>
                    <input type="number" name="title_seo" class="form-control" id="fee">
                </div>
            </div>
            <button type="button" class="btn btn-gradient-primary me-2" id="btn-fee">Cập nhật</button>
        </div>
    </div>
</div>
