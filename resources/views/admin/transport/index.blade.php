@extends('admin.layout.master')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    <div class="page-header">
      <h3 class="page-title"> Quản lý phí vận chuyển </h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
          <li class="breadcrumb-item active" aria-current="page">Quản lý phí vận chuyển</li>
        </ol>
      </nav>
    </div>
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <form id="frm_fee"  method="post">
          {{ csrf_field() }}
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Danh sách</h4>
              <button style="margin:10px 0px;" class="btn btn-info btn-fw trigger">Thêm mới</button>
              <button type="submit  " class="btn btn-gradient-success btn-md btnFee">Cập nhật</button>
              
                <div class="list-fee">

                </div>
              
              <button type="submit" class="btn btn-gradient-success btn-md btnFee">Cập nhật</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@include('admin.transport.modal')
<script>
  $(document).ready(function () {
    load()
    function load(){
      $.ajax({
        url: "{{ route('transport.load') }}",
        method: "POST",
        data: {},
        success:function(data){
          $('.list-fee').html(data);
        }
      })
    }

    $('.trigger').on('click', function () {
      $('.modal-wrapper').toggleClass('open');
      // $('.page-wrapper').toggleClass('blur-it');
    });
    $('#btn-fee').on('click', function () {
      var district = $('#district').val();
      var fee = $('#fee').val();
      $.ajax({
        url: "{{ route('transport.store') }}",
        method: "POST",
        data: {district:district, fee:fee},
        success:function(){
          $('.modal-wrapper').hide();
          load()
          toastr.success("Thêm thành công")
        }
      })
    })

    $('#frm_fee').submit(function(event){
        event.preventDefault();

        var form = $('#frm_fee')[0];
        var formdata = new FormData(form);

        $('.id').each(function(value, index){
            formdata.append("id[]", $(index).val());                        
        }) 
        $('.fee').each(function(value, index){
            formdata.append("fee[]", $(index).val());                        
        })
        $('.district').each(function(value, index){
            formdata.append("district[]", $(index).val());                        
        })

        $.ajax({
            type:"post",
            url: "{{ route('transport.update') }}",
            data:formdata,
            contentType: false,
            processData: false,
            success:function(data){
                if(data == true){
                  load()
                  toastr.success("Cập nhật thành công")
                }
            },
        })
    });

    $(document).on('click','.btn-delete',function(){
      var id = $(this).attr('data-id');
      $.ajax({
        url: "{{ route('transport.delete') }}",
        method: "POST",
        data: {id:id},
        success:function(){
          load()
          toastr.success("Cập nhật thành công")
        }
      })
    })

  });
</script>
@endsection
