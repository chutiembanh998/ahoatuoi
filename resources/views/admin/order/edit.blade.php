@extends('admin.layout.master')
@section('content')
<style>
    .card li{
        font-size:15px;
    }
    .card li span{
        font-size:16px;
        font-weight:bold;
    }
</style>
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row">
        <div class="col-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title">Hóa đơn</h3>
              <div class="row">
                <div class="col-md-6">
                    <h4 class="card-title">Thông tin khách hàng</h4>
                    <ol>
                        <li>Tên KH đặt: <span>{{ $order->name }}</span></li>
                        <li>SĐT KH đặt: <span>{{ $order->tel }}</span></li>
                        <li>Địa chỉ: <span>{{ $order->address }}</span></li>
                        <li>Tên KH nhận: <span>{{ $order->receiver_name }}</span></li>
                        <li>SĐT KH nhận: <span>{{ $order->receiver_tel }}</span></li>
                    </ol>
                </div>
                <div class="col-md-6">
                    <h4 class="card-title">Thông tin hóa đơn</h4>
                    <ol>
                        <li>Mã ĐH: <span>{{ $order->code }}</span></li>
                        <li>Tiền: <span>{{ conversion($order->total_price) }}</span></li>
                        <li>Phí ship: <span>{{ conversion($order->fee) }}</span></li>
                        <li>Tổng tiền: <span>{{ conversion($order->total_price + $order->fee) }}</span></li>
                        <li>PTTT: <span>{{ $order->name }}</span></li>
                        <li>Ghi chú: <span>{{ $order->note }}</span></li>
                        <li>Ngày đặt: <span>{{ $order->created_at }}</span></li>
                    </ol>
                </div>
              </div>
              <div class="row">
                <h4 class="card-title">Danh sách</h4>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Mã SP</th>
                                    <th>Tên SP</th>
                                    <th>Ảnh</th>
                                    <th>Giá</th>
                                    <th>SL</th>
                                    <th>Thành tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order->detail as $key => $row)
                                <tr>
                                    <td>{{ $row->code }}</td>
                                    <td>{{ $row->title }}</td>
                                    <td><img src="{{ $row->avatar }}"></td>
                                    <td>{{ conversion($row->price) }}</td>
                                    <td>{{ $row->quantity }}</td>
                                    <td>{{ conversion($row->price * $row->quantity) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection