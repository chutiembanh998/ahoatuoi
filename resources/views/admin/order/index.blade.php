@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Quản lý Hóa đơn </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Quản lý Hóa đơn</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Danh sách</h4>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Mã ĐH</th>
                                    <th>Tên KH</th>
                                    <th>Tổng giá </th>
                                    <th>Trạng thái đơn</th>
                                    <th>Ngày đặt</th>
                                    <th>Hoạt động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $key => $row)
                                <tr>
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $row->code }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ conversion($row->total_price + $row->fee) }}</td>
                                    <td></td>
                                    <td>{{ $row->created_at }}</td>
                                    <td style="font-size: 20px">
                                        <a href="{{ route('order.edit',['id' => $row->id]) }}"><i class="mdi mdi-border-color"></i></a>
                                        <a href="{{ route('order.delete',['id' => $row->id]) }}"><i class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links('vendor.pagination.admin') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection