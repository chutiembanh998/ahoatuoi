@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Thêm bài viết </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('news.list') }}">Danh sách</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Thêm mới</li>
                </ol>
            </nav>
        </div>
        <form class="forms-sample" action="{{ route('news.store') }}" method="POST" enctype="multipart/form-data" id="myForm">
            @csrf
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Thêm mới</h4>
                            <p class="card-description"> Thông tin chi tiết </p>
                                <div class="form-group">
                                    <label for="title">Tiêu đề</label>
                                    <input type="text" name="title" class="form-control" id="title">
                                    <span class="mess-error" id="title_error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="description">Mô tả</label>
                                    <textarea  name="description" class="form-control" id="description"  rows="6"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="content">Nội dung</label>
                                    <textarea id="content" name="content" class="form-control content"></textarea>
                                    <span class="mess-error" id="content_error"></span>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                      <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="0"> Ẩn <i class="input-helper"></i></label>
                                    </div>
                                    <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="status" value="1" checked> Hiển thị <i class="input-helper"></i></label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>File upload</label>
                                    <input type="file" name="avatar" class="file-upload-default">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled=""
                                            placeholder="Upload Image">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-gradient-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <p class="card-description"> Thông tin SEO </p>
                            <div class="form-group">
                                <label for="title_seo">Tiêu đề</label>
                                <input type="text" name="title_seo" class="form-control" id="title_seo">
                            </div>
                            <div class="form-group">
                                <label for="desc_seo">Mô tả</label>
                                <textarea  name="desc_seo" class="form-control desc_seo" id="desc_seo" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="key_seo">Từ khóa</label>
                                <textarea  name="key_seo" class="form-control key_seo" id="key_seo" rows="6"></textarea>
                            </div>
                            <button type="button" class="btn btn-gradient-primary me-2" id="btn-product">Cập nhật</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('content', {
        filebrowserUploadUrl: "{{route('ckupload', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form',
        height: '500px',
    });

    $('#btn-product').click(function(){
        let error = [];
        var title = $('#title').val();
        var content = CKEDITOR.instances.content.getData();
        if(title == ''){
            $('#title_error').text("Tiêu đề không để trống");
            toastr.error("Tiêu đề không để trống")
            error.push('title');
        }else{
            $('#title_error').text('');
        }

        if(content == ''){
            $('#content_error').text("Nội dung không để trống");
            toastr.error("Nội dung không để trống")
            error.push('content');
        }else{
            $('#content_error').text('');
        }

        if (error.length == 0) {
            $('#myForm').submit();
        }else{
            toastr.error("Xin vui lòng điền đủ thông tin")
        }
    })
</script>
@endsection