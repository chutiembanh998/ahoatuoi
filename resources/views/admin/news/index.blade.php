@extends('admin.layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Quản lý bài viết </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Quản lý bài viết</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Danh sách</h4>
                        <a href="{{ route('news.temp-store') }}" style="margin:10px 0px;" class="btn btn-info btn-fw">Thêm mới</a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tiêu đề</th>
                                    <th>Hình ảnh</th>
                                    <th>Hiển thị</th>
                                    <th>Hoạt động</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($news as $key => $row)
                                <tr>
                                    <th scope="row">{{ ++$key }}</th>
                                    <td>{{ $row->title }}</td>
                                    <td><img src="{{ $row->avatar }}"></td>
                                    <td>{{ $row->status() }}</td>
                                    <td style="font-size: 20px">
                                        <a href="{{ route('news.edit',['id' => $row->id]) }}"><i class="mdi mdi-border-color"></i></a>
                                        <a href="{{ route('news.delete',['id' => $row->id]) }}"><i class="mdi mdi-delete"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $news->links('vendor.pagination.admin') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection