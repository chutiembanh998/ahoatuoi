@if ($paginator->hasPages())

        <ul>
            @if ($paginator->onFirstPage())
            <li class="disabled"><a><i class="ion-ios-arrow-left"></i> Trước</a></li>
            @else
            <li><a class="prev-next prev" href="{{ $paginator->previousPageUrl() }}"><i class="ion-ios-arrow-left"></i> Trước</a></li>
            @endif

            @if($paginator->currentPage() > 2)
            <li><a href="{{ $paginator->url(1) }}">1</a></li>
            @endif
            @if($paginator->currentPage() > 3)
            <li><a>...</a></li>
            @endif
            @foreach(range(1, $paginator->lastPage()) as $i)
            @if($i >= $paginator->currentPage() - 1 && $i <= $paginator->currentPage() + 1)
                @if ($i == $paginator->currentPage())
                <li><a class="active">{{ $i }}</a></li>
                @else
                <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
                @endif
                @endif
                @endforeach
                @if($paginator->currentPage() < $paginator->lastPage() - 2)
                    <li><a>...</a></li>
                    @endif
                    @if($paginator->currentPage() < $paginator->lastPage() - 1)
                        <li><a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a></li>
                        @endif

                        @if ($paginator->hasMorePages())
                        <li><a class="prev-next next" href="{{ $paginator->nextPageUrl() }}">Sau<i class="ion-ios-arrow-right"></i> </a></li>
                        @else
                        <li class="disabled"><i class="fa fa-arrow-right"></i></li>
                        <li class="disabled"><a>Sau<i class="ion-ios-arrow-right"></i></a></li>
                        @endif
        </ul>

@endif