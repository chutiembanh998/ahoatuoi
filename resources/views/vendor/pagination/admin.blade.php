@if ($paginator->hasPages())

<div class="pagination">
    @if ($paginator->onFirstPage())
    <a>&laquo;</a>
    @else
    <a href="{{ $paginator->previousPageUrl() }}">&laquo;</a>
    @endif

    @if($paginator->currentPage() > 2)
    <a href="{{ $paginator->url(1) }}">1</a>
    @endif
    @if($paginator->currentPage() > 3)
    <a>...</a>
    @endif
    @foreach(range(1, $paginator->lastPage()) as $i)
    @if($i >= $paginator->currentPage() - 1 && $i <= $paginator->currentPage() + 1)
        @if ($i == $paginator->currentPage())
        <a class="active">{{ $i }}</a>
        @else
        <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
        @endif
        @endif
        @endforeach
        @if($paginator->currentPage() < $paginator->lastPage() - 2)
            <a>...</a>
            @endif
            @if($paginator->currentPage() < $paginator->lastPage() - 1)
                <a href="{{ $paginator->url($paginator->lastPage()) }}">{{ $paginator->lastPage() }}</a>
                @endif

                @if ($paginator->hasMorePages())
                 <a  href="{{ $paginator->nextPageUrl() }}">&raquo;</a>
                @else
                <a>&raquo;</a>
                @endif
            </div>

@endif