<title class="title-website"> @yield('title')</title>
<meta charset="UTF-8">
<link rel="canonical" href="@yield('canonical')">

<meta http-equiv="content-language" content="vi">
<meta name="geo.region" content="VN">
<meta name="DC.identifier" content="https://vieclamsv.com">
<meta name="robots" content="noodp,index,follow">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="DC.title" content="Việc làm sinh viên">
<meta name="author" content="Việc làm sinh viên">
<meta name="copyright" content="Việc làm sinh viên ">
<meta name="theme-color" content="#4285f4">
<meta name="msapplication-TileColor" content="#4285f4">
<meta name="msapplication-navbutton-color" content="#4285f4">
<meta name="apple-mobile-web-app-status-bar-style" content="#4285f4">

<meta property="og:url" content="@yield('og-url')">
<meta property="og:type" content="website">
<meta property="og:title" content="@yield('og-title')">
<meta property="og:description" content="@yield('og-desc')">
<meta property="og:image" content="@yield('og-image')">
<meta property="og:image:alt" content="vieclamsv">

<meta name="description" content="@yield('seo-desc')">
<meta name="title" content="@yield('seo-title')">
<meta name="keyword" content="@yield('seo-keyword')">
       