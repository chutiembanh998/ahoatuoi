<div class="modal fade" id="productModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <!-- Thumbnail Large Image start -->
                        <div id="pro-img" class="tab-pane fade show active">
                        </div>
                        <!-- Thumbnail Large Image End -->
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="modal-pro-content">
                            <h3 id="title_prm"> </h3>
                            <div class="product-price-wrapper" id="price_prm">
                                
                            </div>
                            <p id="desc_prm"></p>	
                            <div class="product-quantity">
                                <div class="cart-plus-minus">
                                    <input class="cart-plus-minus-box qty_number" type="text" name="qtybutton" value="1">
                                </div>
                                <div class="btn-add">

                                </div>
                                
                            </div>
                            <span><i class="fa fa-check"></i> Còn hàng</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>