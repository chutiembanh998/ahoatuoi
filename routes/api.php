<?php

use App\Http\Controllers\api\BannerApiController;
use App\Http\Controllers\api\CategoryApiController;
use App\Http\Controllers\api\NewsApiController;
use App\Http\Controllers\api\SettingApiController;
use App\Http\Controllers\auth\LoginController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\FeeApiController;
use App\Http\Controllers\api\ProductApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//For Artisan
Route::group(['prefix' => 'artisan'], function () {
    Route::get('optimize', [LoginController::class, 'optimize']);
    Route::get('cache', [LoginController::class, 'cache']);
    Route::get('config', [LoginController::class, 'config']);
    Route::get('storage', [LoginController::class, 'storage']);
    Route::get('migrate', [LoginController::class, 'migrate']);
});

Route::prefix('category')->group(function () {
    Route::get('/list', [CategoryApiController::class, 'index']);
    Route::get('/list-product', [CategoryApiController::class, 'listProduct']);
    Route::get('/edit/{id}', [CategoryApiController::class, 'edit']);
    Route::get('/delete/{id}', [CategoryApiController::class, 'delete']);
    Route::post('/store', [CategoryApiController::class, 'store']);
    Route::post('/update-seo/{id}', [CategoryApiController::class, 'updateSeo']);
});

Route::prefix('banner')->group(function () {
    Route::get('/list', [BannerApiController::class, 'index']);
    Route::get('/edit/{id}', [BannerApiController::class, 'edit']);
    Route::get('/delete/{id}', [BannerApiController::class, 'delete']);
    Route::post('/store', [BannerApiController::class, 'store']);
});

Route::prefix('news')->group(function () {
    Route::get('/list', [NewsApiController::class, 'index']);
    Route::get('/edit/{id}', [NewsApiController::class, 'edit']);
    Route::get('/delete/{id}', [NewsApiController::class, 'delete']);
    Route::post('/store', [NewsApiController::class, 'store']);
    Route::post('/update-seo/{id}', [NewsApiController::class, 'updateSeo']);
});

Route::prefix('setting')->group(function () {
    Route::get('/first', [SettingApiController::class, 'index']);
    Route::post('/update-seo', [SettingApiController::class, 'updateSeo']);
});

Route::prefix('fee')->group(function () {
    Route::get('/list', [FeeApiController::class, 'index']);
    Route::get('/edit/{id}', [FeeApiController::class, 'edit']);
    Route::get('/delete/{id}', [FeeApiController::class, 'delete']);
    Route::post('/store', [FeeApiController::class, 'store']);
});

Route::prefix('product')->group(function () {
    Route::get('/list', [ProductApiController::class, 'index']);
    Route::get('/edit/{id}', [ProductApiController::class, 'edit']);
    Route::get('/delete/{id}', [ProductApiController::class, 'delete']);
    Route::post('/store', [ProductApiController::class, 'store']);
    Route::post('/update-seo/{id}', [ProductApiController::class, 'updateSeo']);
});