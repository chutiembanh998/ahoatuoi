<?php

use App\Http\Controllers\admin\BannerController;
use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\admin\CKEditorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController as PRODUCT;
use App\Http\Controllers\NewsController as NEWS;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\auth\LoginController;

use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\admin\NewsController;
use App\Http\Controllers\admin\OrderController;
use App\Http\Controllers\admin\SettingController;
use App\Http\Controllers\admin\TransportFeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class,'index'])->name('home');
Route::get('/cart.html', [CartController::class, 'tempCart'])->name('cart');
Route::get('/checkout.html', [CheckoutController::class, 'index'])->name('checkout');
Route::view('/login', 'site.login')->name('login');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

// Login Social
Route::get('redirect/{driver}', [LoginController::class, 'redirectToProvider'])
    ->name('login.provider');

Route::get('auth/{driver}/callback', [LoginController::class, 'handleProviderCallback']);


Route::prefix('ajaxRequest')->group(function (){
    Route::post('/product-modal', [PRODUCT::class, 'productDetailModal'])->name('product-modal');
    Route::post('/getProducts', [PRODUCT::class, 'getProducts'])->name('get-products');
    Route::post('/getNews', [NEWS::class, 'getNews'])->name('get-news');
});

// cart
Route::prefix('cart')->group(function (){
    Route::post('/load', [CartController::class, 'loadCart'])->name('cart.load');
    Route::get('/load-total', [CartController::class, 'loadCartTotal'])->name('cart.load-total');
    Route::post('/add', [CartController::class, 'cartSession'])->name('cart.session');
});
// checkout
Route::prefix('checkout')->group(function (){
    Route::post('/store', [CheckoutController::class, 'store'])->name('checkout.store');
    Route::get('/invoice/{code}', [CheckoutController::class, 'invoice'])->name('checkout.invoice');
});


// ADMIN
Route::group(['middleware' => ['role:admin']], function () {
    // Admin
    Route::prefix('admin')->group(function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
        Route::prefix('category')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('category.list');
            Route::get('/store', [CategoryController::class, 'tempStore'])->name('category.temp-store');
            Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
            Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
            Route::get('/delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
        });
        Route::prefix('product')->group(function () {
            Route::get('/', [ProductController::class, 'index'])->name('product.list');
            Route::get('/store', [ProductController::class, 'tempStore'])->name('product.temp-store');
            Route::post('/store', [ProductController::class, 'store'])->name('product.store');
            Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
            Route::get('/delete/{id}', [ProductController::class, 'delete'])->name('product.delete');
        });
        Route::prefix('banner')->group(function () {
            Route::get('/', [BannerController::class, 'index'])->name('banner.list');
            Route::get('/store', [BannerController::class, 'tempStore'])->name('banner.temp-store');
            Route::post('/store', [BannerController::class, 'store'])->name('banner.store');
            Route::get('/edit/{id}', [BannerController::class, 'edit'])->name('banner.edit');
            Route::get('/delete/{id}', [BannerController::class, 'delete'])->name('banner.delete');
        });
        Route::prefix('news')->group(function () {
            Route::get('/', [NewsController::class, 'index'])->name('news.list');
            Route::get('/store', [NewsController::class, 'tempStore'])->name('news.temp-store');
            Route::post('/store', [NewsController::class, 'store'])->name('news.store');
            Route::get('/edit/{id}', [NewsController::class, 'edit'])->name('news.edit');
            Route::get('/delete/{id}', [NewsController::class, 'delete'])->name('news.delete');
        });
        Route::prefix('order')->group(function () {
            Route::get('/', [OrderController::class, 'index'])->name('order.list');
            Route::post('/update-status', [OrderController::class, 'updateStatus'])->name('order.update_status');
            Route::get('/edit/{id}', [OrderController::class, 'edit'])->name('order.edit');
            Route::get('/delete/{id}', [OrderController::class, 'delete'])->name('order.delete');
        });
        Route::prefix('transport-fee')->group(function () {
            Route::get('/', [TransportFeeController::class, 'index'])->name('transport.list');
            Route::post('/load', [TransportFeeController::class, 'load'])->name('transport.load');
            Route::post('/store', [TransportFeeController::class, 'store'])->name('transport.store');
            Route::post('/update', [TransportFeeController::class, 'update'])->name('transport.update');
            Route::post('/delete', [TransportFeeController::class, 'delete'])->name('transport.delete');
        });
        Route::prefix('setting')->group(function () {
            Route::get('/', [SettingController::class, 'index'])->name('setting.list');
            Route::post('/store', [SettingController::class, 'store'])->name('setting.store');
        });
    });

});
// upload image ckeditor
Route::post('ckeditor/image_upload', [CKEditorController::class, 'upload'])->name('ckupload');
// sheet google
Route::get('/bai-viet', [NEWS::class,'tempNews'])->name('news');
Route::get('/bai-viet/{slug}.html', [NEWS::class,'tempNewsDetail'])->name('news.detail');

Route::get('/{slug}', [PRODUCT::class,'tempProducts'])->name('products');
Route::get('/san-pham/{slug}', [PRODUCT::class,'tempProductDetail'])->name('product');