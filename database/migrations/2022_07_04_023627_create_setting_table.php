<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('tel');
            $table->string('mail')->nullable();
            $table->string('address')->nullable();
            $table->text('map')->nullable();
            $table->string('website')->nullable();
            $table->string('fanpage')->nullable();
            $table->string('favicon')->nullable();
            $table->string('logo')->nullable();
            $table->integer('percent_price')->default(0);
            $table->text('analytics')->nullable();
            $table->text('slogan')->nullable();
            $table->text('note')->nullable();
            $table->string('title_seo')->nullable();
            $table->text('desc_seo')->nullable();
            $table->text('key_seo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}
