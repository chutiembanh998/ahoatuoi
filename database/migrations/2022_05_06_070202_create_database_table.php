<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatabaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // PRODUCT
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('avatar')->nullable()->default('default.jpg');
            $table->tinyInteger('level')->default(0);
            $table->Integer('parent_id')->default(0);
            $table->tinyInteger('show')->default(1)->comment('0:Ẩn; 1:Hiển thị');
            $table->tinyInteger('status')->default(0)->comment('0:Bình thường');

            $table->string('title_seo')->nullable();
            $table->text('desc_seo')->nullable();
            $table->text('key_seo')->nullable();

            $table->timestamps();
        });
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('title');
            $table->string('slug');
            $table->string('avatar')->nullable()->default('default.jpg');
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->bigInteger('price');
            $table->bigInteger('promt_price')->nullable()->default(0);
            $table->integer('stock')->default(1)->comment('0:Hết hàng; 1:Còn hàng');
            $table->bigInteger('view')->default(0);
            $table->bigInteger('buyed')->default(0);
            $table->integer('total_review')->default(0);
            $table->tinyInteger('avg_review')->default(0);
            $table->tinyInteger('show')->default(1)->comment('0:Ẩn; 1:Hiển thị');
            $table->tinyInteger('status')->default(0)->comment('0:Bình thường');

            $table->string('title_seo')->nullable();
            $table->text('desc_seo')->nullable();
            $table->text('key_seo')->nullable();

            $table->timestamps();
        });
        Schema::create('category_product', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('product_id');
            $table->timestamps();
        });
        // END PRODUCT
        // NEWS
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('avatar')->nullable()->default('default.jpg');
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->bigInteger('view')->default(0);
            $table->tinyInteger('show')->default(1)->comment('0:Ẩn; 1:Hiển thị');
            $table->tinyInteger('status')->default(0)->comment('0:Bình thường');

            $table->string('title_seo')->nullable();
            $table->text('desc_seo')->nullable();
            $table->text('key_seo')->nullable();

            $table->timestamps();
        });
        // END NEWS
        // BANNER
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type');
            $table->string('title')->nullable();
            $table->string('avatar');
            $table->string('link')->nullable();
            $table->tinyInteger('show')->default(1)->comment('0:Ẩn; 1:Hiển thị');

            $table->timestamps();
        });
        // END BANNER
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
        Schema::dropIfExists('news');
        Schema::dropIfExists('category_product');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
    }
}
