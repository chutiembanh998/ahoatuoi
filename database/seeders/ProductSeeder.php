<?php

namespace Database\Seeders;
use App\Models\Product;
use App\Models\Category;
use App\Models\CategoryProduct;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 6; $i++) { 
            $name = 'Danh mục hoa'.Str::random(4).' '.Str::random(3);
	    	Category::create([
	            'name' => $name,
	            'slug' => Str::slug($name,'-'),
                'avatar' => 'imgc'.$i.'.jpg',
                'status' => random_int(1,3),
	        ]);
    	}
        for ($i=0; $i < 20; $i++) { 
            $title = Str::random(5).' '.Str::random(4).' '.Str::random(3).' '.Str::random(5).' '.Str::random(3);
	    	Product::create([
	            'title' => $title,
                'code' => 'HT'.time().$i,
	            'slug' => Str::slug($title,'-'),
                'avatar' => 'img'.$i.'.jpg',
                'price' => random_int(300,4000).'000',
                'status' => random_int(0,4)
	        ]);
    	}
        for ($i=0; $i < 50; $i++) { 
	    	CategoryProduct::create([
	            'category_id' => random_int(1,6),
	            'product_id' => random_int(1,20),
	        ]);
    	}
    }
}
